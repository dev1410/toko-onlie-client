$(document).ready(function(){

  $(document).on('click', '.img-variant', function(){
    var img = $(this).attr('src');
    $('.product-image img').attr('src', img);
  });

  $(document).on('click', '.img-product', function(){
    var img = $(this).attr('src');
    $('.product-image img').attr('src', img);
  });
});

// ============= click favourite start ===================
$(document).ready(function(){
  $(document).on('click', '.favourite', function(){
    var index = $(this).index('.favourite');
    var checkFavorite = $('.favourite:eq('+index+') i').attr('class');
    if(checkFavorite == "fa fa-heart-o"){
      var style = "color:red";
      var classValue = "fa fa-heart";
    }else{
      var style = "color:#febf67";
      var classValue = "fa fa-heart-o";
    }

    $('.favourite:eq('+index+') i').attr("style", style).attr("class", classValue);
  });
});

// ============= click favourite end ===================

$(document).ready(function(){
  $(document).on('click', '.btn-filter-selected', function(){
    var value = $(this).attr('data-value');
    $(this).remove();
    $('button.btn-size[data-value="'+value+'"]').attr("style", "background-color:#fff; color:#000000");
    $('label[label-value="'+value+'"] input').prop("checked", false);
  });

  $(document).on('click', '.btn-filter-kelompok ', function(){
    var length = $('.filter-kelompok-option label').length;
    for(var i = 0; i < length; i++ ){
      var value = $('.filter-kelompok-option label:eq('+i+')').attr('label-value');
      $('label[label-value="'+value+'"] input').prop("checked", false);
      $('.your-filter-list button[data-value="'+value+'"]').remove();
    }
  });

  $(document).on('click', '.product-option button', function(){
    var value = $(this).text();
    var check = $('.your-filter-list button[data-value="'+value+'"]').length;
    if(1 == check){
      $(this).attr("style", "background-color:#fff; color:#000000");
      $('.your-filter-list button[data-value="'+value+'"]').remove();
    }else{
      $(this).attr("style", "background-color:#febf67; color:#fff");
      var button ='<button class="btn btn-default btn-filter-selected" data-value="'+value+'">'+value+' <i class="fa fa-times"></i></button>';
      $('.your-filter-list').append(button);
    }
  });

  $(document).on('click', '.kelompok-option label, .filter-brand-option label, .filter-color-option label', function(){
    var value = $(this).attr('label-value');
    var check = $(this).find('input').is(":checked");
    var exist = $('.your-filter-list button[data-value="'+value+'"]').length;
    if(check){
      var button ='<button class="btn btn-default btn-filter-selected" data-value="'+value+'">'+value+' <i class="fa fa-times"></i></button>';
      if(exist == 0){
        $('.your-filter-list').append(button);
      }
    }else{
      if(exist == 1){
        $('.your-filter-list button[data-value="'+value+'"]').remove();
      }
    }
  })

});

$(document).ready(function(){
  $('.skin-minimal input').iCheck({
    checkboxClass: 'icheckbox_minimal-orange',
    radioClass: 'iradio_minimal-orange',
    increaseArea: '20%'
  });
});

$(document).ready(function(){
  $(document).on('click', '.product-option-details button', function(){
    $('.product-option-details button').attr("style","background-color:#fff; color:#000000")
    $(this).attr("style", "background-color:#febf67; color:#fff");
  });
});
