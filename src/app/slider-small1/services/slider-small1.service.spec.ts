import { TestBed } from '@angular/core/testing';

import { SliderSmall1Service } from './slider-small1.service';

describe('SliderSmall1Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SliderSmall1Service = TestBed.get(SliderSmall1Service);
    expect(service).toBeTruthy();
  });
});
