import { Injectable } from '@angular/core';
import {Slider} from '../../slider/slider';
import {SLIDERS} from '../mock-slider-small1';

@Injectable({
  providedIn: 'root'
})
export class SliderSmall1Service {

  constructor() { }

  getSliders(gender: string): Slider[] {
    return SLIDERS.filter((slider: Slider) => slider.gender === gender);
  }
}
