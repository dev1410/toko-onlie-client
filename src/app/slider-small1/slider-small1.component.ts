import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Slider} from '../slider/slider';
import {SliderService} from '../slider/services/slider.service';
import {takeUntil} from 'rxjs/operators';
import {componentDestroyed} from '@w11k/ngx-componentdestroyed';

@Component({
  selector: 'app-slider-small1',
  templateUrl: './slider-small1.component.html',
  styleUrls: ['./slider-small1.component.css']
})
export class SliderSmall1Component implements OnInit, OnDestroy {
  sliders: Slider[];

  @Input()
  gender: string;

  constructor(private sliderService: SliderService) { }

  ngOnInit() {
    this.getSliders();
  }

  getSliders(): void {
      this.sliderService.getSliders('small', this.gender)
        .pipe(takeUntil(componentDestroyed(this)))
        .subscribe((sliders: Slider[]) => {
          this.sliders = sliders;
          this.sliders.forEach((s: Slider) => s.image = this.sliderService.getImage(s.image));
      });
  }

  ngOnDestroy(): void {
  }

}
