import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SliderSmall1Component } from './slider-small1.component';

describe('SliderSmall1Component', () => {
  let component: SliderSmall1Component;
  let fixture: ComponentFixture<SliderSmall1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SliderSmall1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SliderSmall1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
