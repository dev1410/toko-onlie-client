import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {WishlistService} from './wishlist.service';
import {Pagination} from './pagination';
import {Productservice} from '../products/services/product.service';
import { takeUntil } from 'rxjs/operators';
import { componentDestroyed } from '@w11k/ngx-componentdestroyed';
import {Title} from '@angular/platform-browser';
import {ProductTile} from '../products/product-tile';


@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.css']
})
export class WishlistComponent implements OnInit, OnDestroy {
  private listSubscribe;
  items = [];
  list_item: ProductTile[];
  gender: string;
  subcategory: string;
  group: string;
  msg: string;
  totalPages =  1;
  pageSize = 10;
  current_page = 1;


  constructor(
    public wishlistService: WishlistService,
    private productService: Productservice,
    private titleService: Title) { }

  ngOnInit() {
    this.gender = localStorage.getItem('__gdr') === '1' ? 'pria' : 'wanita';
    this.titleService.setTitle('Daftar Keinginan');
    this.wishlistService.checkTotal();
    setTimeout(() => {
      this.all();
    }, 500)
  }

  all(page= null) {
    this.wishlistService.all(page)
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe(
      (httpResponse: Pagination) => {
        if (httpResponse.data.length > 0) {
          this.items = Object.assign([], httpResponse);
          this.totalPages = httpResponse.last_page;
          this.current_page = httpResponse.current_page;
          const product_ids = httpResponse.data.map((c: any) => c.product_id);
          this.productService.getProductsByIdList(product_ids)
            .pipe(takeUntil(componentDestroyed(this)))
            .subscribe((d: any) => {
              this.list_item = d.data;
              this.items['data'].map((item: any) => {
                const i = this.list_item.findIndex((_item: ProductTile) => _item.product_id === item.product_id);
                if (i > -1) {
                  this.list_item[i].stok = item.product_variant.stock;
                }
              });
              this.wishlistService.wishlist = httpResponse.total;
            });
        } else { 
          if( this.wishlistService.wishlist != 0){
            setTimeout(() => {
              this.all(page - 1);
            },500)
          } 
        }
      }
    );
  }
  deleteWishlist(product_id: number): void {
    this.wishlistService.delete(product_id)
    .pipe(takeUntil(componentDestroyed(this)))
    .subscribe((ress: any) => {
      this.list_item.splice(
        this.list_item.findIndex(_item => _item.product_id === product_id), 1);
      if(this.list_item.length < 10){
        this.all(this.current_page);
      }
    });
    
    
  }

  ngOnDestroy(): void {
    if (this.listSubscribe) {
      this.listSubscribe.unsubscribe();
    }
  }
}
