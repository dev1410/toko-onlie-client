import {Injectable, OnDestroy} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {takeUntil} from 'rxjs/operators';
import {componentDestroyed} from '@w11k/ngx-componentdestroyed';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class WishlistService implements OnDestroy {
  wishlist = 0;
  isWishlist = false;
  private onDetail: number;
  
  private API_URL = environment.apiUrl;
  constructor(private router: Router, private http: HttpClient) {
    if (localStorage.getItem('__csrf')) {
      this.all()
        .pipe(takeUntil(componentDestroyed(this)))
        .subscribe((c: any) => this.wishlist = c.total);
    }
  }

  add(product_id: number) {
    const endpoint = `${this.API_URL}/wishlist/add`;
    this.wishlist += 1;
    return this.http.post(endpoint, {'product_id': product_id});
  }

  delete(product_id: number) {
    const endpoint = `${this.API_URL}/wishlist/delete`;
    this.wishlist -= 1;
    return this.http.post(endpoint, {'product_id': product_id});
  }

  all(page= null) {
    if (page != null){
      const endpoint = `${this.API_URL}/wishlist/data/list?page=${page}`;
      return this.http.get(endpoint);
    } else  {
      const endpoint = `${this.API_URL}/wishlist/data/list`;
      return this.http.get(endpoint);
    }
  }

  getAll(){
    const endpoint = `${this.API_URL}/wishlist/products`;
    return this.http.get(endpoint);
  }

  setOnDetail(product_id: number){
    this.onDetail = product_id;
  }

  getOnDetail(){
    return this.onDetail;
  }

  setToggle(wishlist: boolean){
    this.isWishlist = !wishlist;
  }

  ngOnDestroy(): void {
  }

  redirectToLoginPage(){
    this.router.navigate(["pengguna/akun/masuk"]);
  }

  checkTotal(){
    if (localStorage.getItem('__csrf')) {
      this.all()
        .pipe(takeUntil(componentDestroyed(this)))
        .subscribe((c: any) => this.wishlist = c.total);
    }
  }

}
