import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ProductsComponent} from './products/products.component';
import {SliderComponent} from './slider/slider.component';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {HomeComponent} from './home/home.component';
import {SliderSmall1Component} from './slider-small1/slider-small1.component';
import {SliderSmall2Component} from './slider-small2/slider-small2.component';
import {BannerComponent} from './banner/banner.component';
import {ProductSliderComponent} from './products/product-slider/product-slider.component';
import {ProductTrendingComponent} from './products/product-trending/product-trending.component';
import {ProductCategoryComponent} from './products/product-category/product-category.component';
import {SubscriberComponent} from './subscriber/subscriber.component';
import {NavbarComponent} from './header/navbar/navbar.component';
import {ProductFilterComponent} from './products/product-filter/product-filter.component';
import {SearchBarComponent} from './header/search-bar/search-bar.component';
import {ProductDetailComponent} from './products/product-detail/product-detail.component';
import {BreadcrumbComponent} from './breadcrumb/breadcrumb.component';
import {SlugToTitleCasePipe} from './shared/pipes/slug-to-title-case.pipe';
import {BreadcrumbDirective} from './breadcrumb/breadcrumb.directive';
import {LoadingBarHttpClientModule} from '@ngx-loading-bar/http-client';
import {CartStikyHeaderComponent} from './cart/cart-stiky-header/cart-stiky-header.component';
import {CartComponent} from './cart/cart.component';
import {CheckoutComponent} from './cart/checkout/checkout.component';
import {CartService} from './cart/cart.service';
import {UserService} from './user/user.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {WishlistComponent} from './wishlist/wishlist.component';
import {WishlistService} from './wishlist/wishlist.service';
import {TokenInterceptor} from './user/auth/token-interceptor';
import {InputNumberOnlyDirective} from './shared/directives/input-number-only.directive';
import {TypeBayarPipe} from './shared/pipes/type-bayar.pipe';
import {WrapImagePipe} from './shared/pipes/wrap-image.pipe';
import {CheckoutSuccessComponent} from './cart/checkout-success/checkout-success.component';
import {CardNumberPipe} from './shared/pipes/card-number.pipe';
import {IcheckDirective} from './shared/directives/icheck.directive';
import {DepartmentService} from './shared/services/department.service';
import {RouterService} from './router.service';
import {SubkelompokComponent} from './products/subkelompok/subkelompok.component';
import {SubkelompokDirective} from './products/subkelompok/subkelompok.directive';
import {ProductTilesComponent} from './products/product-tile/product-tiles.component';
import {ProductTilesDirective} from './products/product-tile/product-tiles.directive';
import {BrandOptionsComponent} from './products/brand-options/brand-options.component';
import {BrandOptionsDirective} from './products/brand-options/brand-options.directive';
import {SizeOptionsComponent} from './products/size-options/size-options.component';
import {SizeOptionsDirective} from './products/size-options/size-options.directive';
import {ColorOptionsComponent} from './products/color-options/color-options.component';
import {ColorOptionsDirective} from './products/color-options/color-options.directive';
import {PriceRangeComponent} from './products/price-range/price-range.component';
import {PriceRangeDirective} from './products/price-range/price-range.directive';
import {PaginationComponent} from './products/pagination/pagination.component';
import {PaginationDirective} from './products/pagination/pagination.directive';
import {ProductSliderDirective} from './products/product-slider/product-slider.directive';
import {ProductSearchComponent} from './products/product-search/product-search.component';
import {SubdepartmentsComponent} from './products/subdepartments/subdepartments.component';
import {SubdepartmentComponent} from './products/subdepartment/subdepartment.component';
import {SubdepartmentDirective} from './products/subdepartment/subdepartment.directive';
import {SubdepartmentsDirective} from './products/subdepartments/subdepartments.directive';
import {StartkitComponent} from './startkit/startkit.component';
import {StartkitService} from './startkit/startkit.service';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {LazyLoadImagesModule} from 'ngx-lazy-load-images';
import {GlobalModule} from './shared/global/global.module';
import {registerLocaleData} from '@angular/common';
import localeId from '@angular/common/locales/id';
import {ImageZoomModule} from 'angular2-image-zoom';
import { SwitchBrokenCdnLinkDirective } from './shared/directives/switch-broken-cdn-link.directive';
import { FixBrokenCdnLinkPipe } from './shared/pipes/fix-broken-cdn-link.pipe';

registerLocaleData(localeId);

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    SliderComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    SliderSmall1Component,
    SliderSmall2Component,
    BannerComponent,
    ProductSliderComponent,
    ProductTrendingComponent,
    ProductCategoryComponent,
    SubscriberComponent,
    NavbarComponent,
    ProductFilterComponent,
    SearchBarComponent,
    ProductDetailComponent,
    BreadcrumbComponent,
    SlugToTitleCasePipe,
    BreadcrumbDirective,
    CartStikyHeaderComponent,
    CartComponent,
    CheckoutComponent,
    CheckoutSuccessComponent,
    InputNumberOnlyDirective,
    WishlistComponent,
    InputNumberOnlyDirective,
    TypeBayarPipe,
    WrapImagePipe,
    CardNumberPipe,
    IcheckDirective,
    SubkelompokComponent,
    SubkelompokDirective,
    ProductTilesComponent,
    ProductTilesDirective,
    BrandOptionsComponent,
    BrandOptionsDirective,
    SizeOptionsComponent,
    SizeOptionsDirective,
    ColorOptionsComponent,
    ColorOptionsDirective,
    PriceRangeComponent,
    PriceRangeDirective,
    PaginationComponent,
    PaginationDirective,
    ProductSliderDirective,
    ProductSearchComponent,
    SubdepartmentsComponent,
    SubdepartmentComponent,
    SubdepartmentDirective,
    SubdepartmentsDirective,
    StartkitComponent,
    PageNotFoundComponent,
    SwitchBrokenCdnLinkDirective,
    FixBrokenCdnLinkPipe,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    GlobalModule,
    ImageZoomModule,
    LoadingBarHttpClientModule,
    LazyLoadImagesModule,
    AppRoutingModule,
  ],
  providers: [
    RouterService,
    TokenInterceptor,
    DepartmentService,
    CartService,
    UserService,
    WishlistService,
    StartkitService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
  ],
  entryComponents: [
    BreadcrumbComponent,
    SubkelompokComponent,
    ProductTilesComponent,
    BrandOptionsComponent,
    ColorOptionsComponent,
    SizeOptionsComponent,
    PriceRangeComponent,
    PaginationComponent,
    ProductSliderComponent,
    SubdepartmentsComponent,
    StartkitComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
