import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SliderSmall2Component } from './slider-small2.component';

describe('SliderSmall2Component', () => {
  let component: SliderSmall2Component;
  let fixture: ComponentFixture<SliderSmall2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SliderSmall2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SliderSmall2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
