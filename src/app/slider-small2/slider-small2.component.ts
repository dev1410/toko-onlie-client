import {Component, Input, OnInit} from '@angular/core';
import {Slider} from '../slider/slider';
import {SliderService} from '../slider/services/slider.service';

@Component({
  selector: 'app-slider-small2',
  templateUrl: './slider-small2.component.html',
  styleUrls: ['./slider-small2.component.css']
})
export class SliderSmall2Component implements OnInit {
  sliders: Slider[];

  @Input()
  gender: string;

  constructor(private sliderService: SliderService) { }

  ngOnInit() {
    this.getSliders();
  }

  getSliders(): void {
    this.sliderService.getSliders("small-right", this.gender).subscribe((sliders: Slider[]) => {
        this.sliders = sliders;
        this.sliders.forEach((s: Slider) => s.image = this.sliderService.getImage(s.image));
    });
  }

}
