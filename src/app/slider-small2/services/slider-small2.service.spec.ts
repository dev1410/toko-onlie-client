import { TestBed } from '@angular/core/testing';

import { SliderSmall2Service } from './slider-small2.service';

describe('SliderSmall2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SliderSmall2Service = TestBed.get(SliderSmall2Service);
    expect(service).toBeTruthy();
  });
});
