import {Injectable, OnDestroy} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Accept':  'application/json'
  })
};

const API_URL = environment.apiUrl;
const IMAGE_URL = environment.imageUrl;

@Injectable({
  providedIn: 'root'
})
export class HttpWrapper {
    constructor(private http: HttpClient) {
    }

    get<T>(path: string, params: Object)  {
        let httpParams = new HttpParams(params);
        let head = {...httpOptions, params: httpParams};
        head.headers.append('Content-type', 'application/json');
        return this.http.get<T>(API_URL + path, head);
    }

    post<T>(path: string, data: Object) {
        if (!data) {
            data = {};
        }
        let head = {...httpOptions};
        head.headers.append('Content-type', 'application/x-www-form-urlencoded');
        return this.http.post<T>(API_URL + path, data, head);
    }

    wrapImage(path: string): string {
        let imgUrl = IMAGE_URL.replace(/[\/]+$/, '');
        if (null != path && path.match(/^https?:\/\//)) {
            return path.replace(/^https?:\/\/[^\/]+\/img\//, imgUrl + '/img/');
        }

        return imgUrl + '/' + (path.replace(/^[\/]+/, ''));
    }
}
