import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CustomCurrencyPipe} from '../pipes/custom-currency.pipe';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SweetAlert2Module} from '@toverux/ngx-sweetalert2';

@NgModule({
  declarations: [CustomCurrencyPipe],
  imports: [
    CommonModule,
    SweetAlert2Module.forRoot(),
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SweetAlert2Module,
    CustomCurrencyPipe,
  ]
})
export class GlobalModule { }
