import {Directive, ElementRef, OnInit} from '@angular/core';

declare var $: any;

@Directive({
  selector: '[appIcheck]'
})
export class IcheckDirective implements OnInit {

  constructor(public el: ElementRef) {
  }

  ngOnInit(): void {
    $(this.el.nativeElement).iCheck({
      checkboxClass: 'icheckbox_square-orange',
      radioClass: 'iradio_square-orange',
      increaseArea: '20%'
    });
  }
}
