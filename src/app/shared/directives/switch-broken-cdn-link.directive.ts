import {Directive, Input} from '@angular/core';
import {FixBrokenCdnLinkPipe} from '../pipes/fix-broken-cdn-link.pipe';

@Directive({
  selector: 'img[default]',
  host: {
    '(error)': 'updateUrl()',
    '[src]': 'src'
  }
})
export class SwitchBrokenCdnLinkDirective {
  @Input() src: string;
  @Input() default: string;

  constructor() { }

  updateUrl() {
    this.src = new FixBrokenCdnLinkPipe().transform(this.default);
  }

}
