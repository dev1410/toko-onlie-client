import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {Subdepartment} from '../../header/navbar/subdepartment';
import {Subkelompok} from '../../products/subkelompok';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService {
  API_URL = environment.apiUrl;
  subdepartments$: Observable<Subdepartment[]>;

  constructor(private http: HttpClient) {

    let gender = localStorage.getItem('__gdr');
    if (gender && !this.subdepartments$) {
      gender = gender === '1' ? 'pria' : 'wanita';
      this.subdepartments$ = this.getSubdepartments(gender);
    }
  }

  getSubdepartments(gender: string): Observable<any> {
    return this.http.get(`${this.API_URL}/master/subdepartmens/${gender}`);
  }

  getSubdepartment(slug: string): Observable<any> {
    return this.http.get(`${this.API_URL}/master/subdepartmens/get/${slug}`);
  }

  setSubdepartments(gender: string) {
    this.subdepartments$ = this.getSubdepartments(gender);
  }

  getGroups(subdept_id: number): Observable<any> {
    return this.http.get(`${this.API_URL}/master/subdepartmens/get/${subdept_id}/groups`);
  }
}
