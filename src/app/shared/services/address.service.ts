import { Injectable } from '@angular/core';
import {Address} from '../models/address';
import {HttpWrapper} from '../../_services/http-wrapper';
import {Observable} from 'rxjs';
import {AddressSuggestion} from '../models/address-suggestion';
import {ShippingOption} from '../../cart/checkout/shipping-option';

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  constructor(private httpWrapper: HttpWrapper) { }

  add(address: Address): Observable<Address> {
    return this.httpWrapper.post<Address>('/user/address/add', address);
  }

  update(address: Address): Observable<Address|any> {
    return this.httpWrapper.post<Address>('/user/address/edit', address);
  }

  delete(address: Address) {
    return this.httpWrapper.post('/user/address/delete', {address_id: address.address_id});
  }

  list(): Observable<Address[]> {
    return this.httpWrapper.get<Address[]>('/user/address/list', {});
  }

  get(address_id: number): Observable<Address> {
    return this.httpWrapper.get<Address>(`/user/address/details/${address_id}`, {});
  }

  getPrimary(): Observable<Address> {
    return this.httpWrapper.get<Address>(`/user/address/primary`, {});
  }

  setPrimary(address: Address) {
    return this.httpWrapper.post('/user/address/setting/primary', {address_id: address.address_id});
  }

  getSuggestion(query: string): Observable<AddressSuggestion[]> {
    return this.httpWrapper.post<AddressSuggestion[]>('/user/address/suggestion', {search: query});
  }

  simulateShippingCost(district_id: number, weight: number): Observable<ShippingOption[]> {
    return this.httpWrapper.post<ShippingOption[]>('/city/calculate/expedition', {district_id: district_id, weight: weight});
  }
}
