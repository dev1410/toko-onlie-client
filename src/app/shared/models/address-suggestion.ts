export class AddressSuggestion {
  district_id: number;
  city_id: number;
  village_id: number;
  village_name: string;
  name_text: string;
}
