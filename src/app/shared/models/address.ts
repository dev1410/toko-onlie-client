export class Address {
  address_id: number;
  address_name: string;
  receiver_name: string;
  hp: string;
  user_id: number;
  province_id: number;
  district_id: number;
  city_id: number;
  village_id: number;
  postal_code: number;
  address: string;
  is_primary: boolean;
  province_name: string;
  city_name: string;
  district_name: string;
  village_name: string;
}
