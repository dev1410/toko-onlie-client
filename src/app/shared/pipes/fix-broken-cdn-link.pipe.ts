import { Pipe, PipeTransform } from '@angular/core';
import {environment} from '../../../environments/environment';

@Pipe({
  name: 'fixBrokenCdnLink'
})
export class FixBrokenCdnLinkPipe implements PipeTransform {

  transform(value: string): string {
    if (value.includes('siata.sgp1.cdn.digitaloceanspaces.com')) {
      return value.replace('https://siata.sgp1.cdn.digitaloceanspaces.com', environment.imageUrl);
    }
    return null;
  }

}
