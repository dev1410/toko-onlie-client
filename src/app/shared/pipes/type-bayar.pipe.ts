import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'typeBayar'
})
export class TypeBayarPipe implements PipeTransform {

  transform(value: number): string {
    if (value === 1) {
      return 'Transfer Manual';
    } else if (value === 2) {
      return 'Credit Card/Debit Card';
    } else {
      return 'Virtual Account';
    }
  }

}
