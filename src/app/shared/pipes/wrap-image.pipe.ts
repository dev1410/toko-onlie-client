import { Pipe, PipeTransform } from '@angular/core';
import {environment} from '../../../environments/environment';

@Pipe({
  name: 'wrapImage'
})
export class WrapImagePipe implements PipeTransform {

  transform(value: string): string {
      var result = value;
      if (value) {
          if (value.includes('toko.online')) {
              result = value.replace('toko.online', '182.16.178.172/toko-online');
          } else if (!value.match(/^https?/)) {
              result = [environment.imageUrl, value].join('/');
          }
      }
      return result;
  }

}
