import { Pipe, PipeTransform } from '@angular/core';
import { TitleCasePipe } from '@angular/common';

@Pipe({
  name: 'slugToTitleCase'
})
export class SlugToTitleCasePipe implements PipeTransform {

  transform(value: string): string {
    const words = value.split('-');
    return new TitleCasePipe().transform(words.join(' '));
  }

}
