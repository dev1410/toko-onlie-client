import {FormGroup, ValidationErrors, ValidatorFn} from '@angular/forms';

export const passwordMatchValidator: ValidatorFn = (control: FormGroup):
ValidationErrors | null => {
  const password = control.get('password').value;
  const password_confirm = control.get('password_confirm').value;

  if (control.get('password_confirm').dirty || control.get('password_confirm').touched) {
    return password === password_confirm ? null : {'passwordMatch': true};
  }
  return null;
};
