export class CheckoutSuccess {
  bank_name: string;
  code_cost_unique: number;
  logo_bank: string;
  product_cost: number;
  rekening_name: string;
  rekening_number: string;
  total_cost: number;
  transfer_guide: [];
  virtual_number: string;
  expired_time: string;
}
