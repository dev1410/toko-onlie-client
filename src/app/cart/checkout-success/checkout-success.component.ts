import {Component, OnDestroy, OnInit} from '@angular/core';
import {CheckoutSuccess} from './checkout-success';
import {TransferGuide} from './transfer-guide';

declare const $: any;

@Component({
  selector: 'app-checkout-success',
  templateUrl: './checkout-success.component.html',
  styleUrls: ['./checkout-success.component.css']
})
export class CheckoutSuccessComponent implements OnInit, OnDestroy {
  transaction: CheckoutSuccess;
  transferGuides: TransferGuide[];

  constructor() { }

  ngOnInit() {
    const data = JSON.parse(localStorage.getItem('checkoutSuccessData'));

    this.transaction = data.data;
    this.transferGuides = data.data.transfer_guide;

    this.calculateCountDown();
  }

  calculateCountDown(): void {
    const date = new Date(this.transaction.expired_time);
    const countDownDate = date.getTime();

    const monthNames = [
      'Januari', 'Februari', 'Maret',
      'April', 'Mei', 'Juni', 'Juli',
      'Agustus', 'September', 'Oktober',
      'November', 'Desember'
    ];

    const hari = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Juma\'at', 'Sabtu'];
    const dayName = hari[date.getDay()];

    const day = date.getDate();
    const monthIndex = date.getMonth();
    const monthName = monthNames[monthIndex];
    const year = date.getFullYear();
    const jam = date.getHours();
    const menit = date.getMinutes();

    const expired = `Sebelum ${dayName} ${day} ${monthName} ${year} pukul ${jam}:${menit} WIB`;

    $('.expired-time').text(expired);

    // Update the count down every 1 second
    setInterval(function() {
      // Get todays date and time
      const now = new Date().getTime();
      // Find the distance between now an the count down date
      const distance = countDownDate - now;
      // Time calculations for days, hours, minutes and seconds
      const days = Math.round(distance / (1000 * 60 * 60 * 24));
      const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60) * days);
      let minutes = String(Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)));
      let seconds = String(Math.floor((distance % (1000 * 60)) / 1000));

      if (seconds.length === 1) {
        seconds = '0' + seconds;
      }

      if (minutes.length === 1) {
        minutes = '0' + minutes;
      }

      $('#hours').text(hours);
      $('#minutes').text(minutes);
      $('#seconds').text(seconds);
    }, 1000);
  }

  ngOnDestroy(): void {
    localStorage.removeItem('checkoutSuccessData');
  }

}
