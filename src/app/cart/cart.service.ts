import {EventEmitter, Injectable, OnDestroy} from '@angular/core';
import {CartItem} from './cart-item';
import {environment} from '../../environments/environment';
import {UserService} from '../user/user.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {takeUntil} from 'rxjs/operators';
import {componentDestroyed} from '@w11k/ngx-componentdestroyed';

const httpOptions = {
  headers: new HttpHeaders({
    'Accept':  'application/json',
    'Content-Type':  'application/x-www-form-urlencoded',
  })
};

@Injectable({
  providedIn: 'root'
})
export class CartService implements OnDestroy {
  API_URL = environment.apiUrl;
  items$: EventEmitter<CartItem[]> = new EventEmitter(true);
  items: CartItem[] = [];
  confirmedItems: CartItem[] = [];
  private loggedIn = false;
  private loggedInSubscription;
  private getDataSubscription;
  private addSubscription;
  private deleteSubscription;
  synchronising = new EventEmitter(true);

  constructor(private userService: UserService,
              private http: HttpClient) {
    const existing_items = localStorage.getItem('cartItems');
    const existing_confirmed_items = localStorage.getItem('confirmedItems');

    if (existing_items) {
      this.items = JSON.parse(existing_items);
      this.items$.emit(JSON.parse(existing_items));
    }

    if (existing_confirmed_items) {
      this.confirmedItems = JSON.parse(existing_confirmed_items);
    }

    this.loggedIn = this.userService.isLoggedIn();

    if (this.loggedIn) {
      this.getDataSubscription = this.syncData();
    }

  }

  add(item: CartItem): void {

    let isAdded = false;

    const i = this.items.findIndex((cartItem: CartItem) => cartItem.variant_id === item.variant_id);
    if (i > -1) {
      if (item.quantity >= this.items[i].variant_stock) {
        return;
      } else {
        if ((item.quantity + this.items[i].quantity) > this.items[i].variant_stock) {
          this.items[i].quantity = this.items[i].variant_stock;
          isAdded = false;
        } else {
          this.items[i].quantity += item.quantity;
          isAdded = true;
        }
      }
    } else {
      isAdded = true;
    }

    if (isAdded) {
      if (this.loggedIn) {
        if (this.addSubscription) {
          this.addSubscription.unsubscribe();
        }
        this.addSubscription = this.addToServer(item.variant_id, item.quantity)
          .pipe(takeUntil(componentDestroyed(this)))
          .subscribe((resp: any) => {
            if (resp.code === 1) {
              item.id = resp.data;
              if (i === -1) {
                this.items.push(item);
              }
              this.setLocalStorage();
            }
          });
      } else {
        if (i === -1) {
          this.items.push(item);
        }
        this.setLocalStorage();
      }
    }
  }

  addToServer(variant_id: number, qty: number) {
    const endpoint = `${this.API_URL}/shopping/cart/add`;
    return this.http.post(endpoint, {product_variant_id: variant_id, qty: qty});
  }

  delete(variant_id: number, cartId: number = null): void {
    const i = this.items.findIndex((cartItem: CartItem) => cartItem.variant_id === variant_id);

    if (this.loggedIn && cartId) {
      this.deleteToServer([cartId])
        .pipe(takeUntil(componentDestroyed(this)))
        .subscribe(
        (next: any) => {
          if (next.code === 1) {
            if (i > -1) {
              this.items.splice(i, 1);
              this.setLocalStorage();
            }
          }
        });
    } else {
      if (i > -1) {
        this.items.splice(i, 1);
      }
      this.setLocalStorage();
    }
  }

  deleteToServer(cartId: number[]) {
    const endpoint = `${this.API_URL}/shopping/cart/delete`;
    return this.http.post(endpoint, {shopping_cart_id: cartId, address_id: 1}, httpOptions);
  }

  increase(item: CartItem) {
    if (item.quantity < item.variant_stock) {
      const i = this.items.findIndex(_item => _item.id === item.id);
      if (i > -1) {
        this.items[i].quantity += 1;
      }

      this.setLocalStorage();

      if (this.loggedIn) {
        const endpoint = `${this.API_URL}/shopping/cart/add`;
        return this.http.post(endpoint, {product_variant_id: item.variant_id, qty: 1})
          .pipe(takeUntil(componentDestroyed(this)))
          .subscribe();
      }
    }
    return item;
  }

  decrease(item: CartItem) {

    if (item.quantity > 1) {
      const i = this.items.findIndex(_item => _item.id === item.id);
      if (i > -1) {
        this.items[i].quantity -= 1;
      }

      this.setLocalStorage();

      if (this.loggedIn) {
        const endpoint = `${this.API_URL}/shopping/cart/minus`;
        return this.http.post(endpoint, {product_variant_id: item.variant_id})
          .pipe(takeUntil(componentDestroyed(this)))
          .subscribe();
      }
    }
    return item;
  }

  update(variant_id: number, qty: number, cartId: number = null): void {
    const i = this.items.findIndex((cartItem: CartItem) => cartItem.variant_id === variant_id);

    if (qty <= this.items[i].variant_stock) {

      if (this.loggedIn && cartId) {

        if (this.items[i].quantity > qty) {
          // TODO: decrease quantity through API. API aren't ready.
        } else {
          if (this.addSubscription) {
            this.addSubscription.unsubscribe();
          }
          this.addSubscription = this.addToServer(variant_id, qty - this.items[i].quantity)
            .pipe(takeUntil(componentDestroyed(this)))
            .subscribe();
        }
      }

      this.items[i].quantity = qty;
    }

    this.setLocalStorage();
  }

  clear(): void {
    this.items = [];
    this.confirmedItems = [];
    localStorage.removeItem('cartItems');
    localStorage.removeItem('confirmedItems');
  }

  clearConfirmed(): void {
    localStorage.removeItem('confirmedItems');
  }

  total(): number {
    if (this.items) {
      return this.items.length;
    }
  }

  setLocalStorage(): void {
    if (this.items.length > 0) {
      localStorage.setItem('cartItems', JSON.stringify(this.items));
    } else {
      localStorage.removeItem('cartItems');
    }
  }

  setConfirmedItems(items: CartItem[]) {
    this.confirmedItems = Object.assign([], items);

    if (this.confirmedItems.length > 0) {
      localStorage.setItem('confirmedItems', JSON.stringify(this.confirmedItems));
    } else {
      localStorage.removeItem('confirmedItems');
    }
  }

  syncData() {
    const endpoint = `${this.API_URL}/shopping/cart/data/list`;
    this.synchronising.emit('start');

    return this.http.get(endpoint)
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe((value: any) => {
        this.items.map(item => {
          const i = value.data.findIndex(c => c.product_variant_id === item.variant_id);

          if (i === -1) {
            if (this.addSubscription) {
              this.addSubscription.unsubscribe();
            }
            this.addSubscription = this.addToServer(item.variant_id, item.quantity)
              .pipe(takeUntil(componentDestroyed(this)))
              .subscribe();
          } else {
            const itemDB = value.data[i];
            if (itemDB.qty < item.quantity < itemDB.product_variant.stock) {
              value.data[i].qty = item.quantity;
            }
          }
        });

        this.clear();

        value.data.map(item => {
          const cartItem = new CartItem;
          cartItem.id = item.id;
          cartItem.product_id = item.product_variant.product_id;
          cartItem.product_name = item.product_variant.product.name;
          cartItem.product_image = item.product_variant.all_image[0].image;
          cartItem.product_brand = item.product_variant.product.brand;
          cartItem.product_gender = item.product_variant.product.tag;
          cartItem.product_subcategory = item.product_variant.product.sub_category_id;
          cartItem.product_group = item.product_variant.product.sub_category_id;
          cartItem.product_slug = item.product_variant.product.slug;

          cartItem.variant_id = item.product_variant_id;
          cartItem.variant_text = `(${item.product_variant.variant_details.map(variant_detail =>
            `${variant_detail.variant_category_name}: ${variant_detail.variant_name}`).join(',')})`;

          cartItem.variant_image = item.product_variant.image;
          cartItem.variant_stock = item.product_variant.stock;
          cartItem.price = item.product_variant.discount ? item.product_variant.promo_price : item.product_variant.price;
          cartItem.discount = item.product_variant.discount;
          cartItem.discount_price = item.product_variant.promo_price;
          cartItem.quantity = item.qty;
          cartItem.weight = item.product_variant.product.weight;

          this.items.push(cartItem);
          this.setLocalStorage();
        });
        this.synchronising.emit('done');
      });
  }

  ngOnDestroy(): void {
    if (this.loggedIn) {
      this.getDataSubscription.unsubscribe();
    }

    if (this.addSubscription) {
      this.addSubscription.unsubscribe();
    }

    if (this.deleteSubscription) {
      this.deleteSubscription.unsubscribe();
    }

    if (this.loggedInSubscription) {
      this.loggedInSubscription.unsubscribe();
    }

    localStorage.removeItem('confirmedItems');
  }
}
