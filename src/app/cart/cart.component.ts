import {Component, OnDestroy, OnInit} from '@angular/core';
import {CartService} from './cart.service';
import {CartItem} from './cart-item';
import {Title} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {takeUntil} from 'rxjs/operators';
import {componentDestroyed} from '@w11k/ngx-componentdestroyed';
import {Productservice} from '../products/services/product.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit, OnDestroy {
  items: CartItem[] = [];
  selectedItems: CartItem[] = [];
  cartGrandTotal = 0;

  isChecked = {};
  isCheckAll = false;
  private clickedProduct: any;
  private gender = localStorage.getItem('__gdr') === '1' ? 'pria' : 'wanita';

  constructor(public cartService: CartService,
              private titleService: Title,
              private router: Router,
              private productService: Productservice) {
  }

  ngOnInit() {
    if (localStorage.getItem('__csrf')) {
      this.cartService.synchronising.emit('initialize');
      this.cartService.syncData();

      this.cartService.synchronising.subscribe(state => {
        // wait for sync state done
        if (state === 'done') {
          this.onInitProcess();
        }
      });
    } else {
      this.onInitProcess();
    }
  }

  onInitProcess() {
    this.items = this.cartService.items;
    const title = this.items.length > 0 ? `(${this.items.length}) Troli Pembelian` : 'Troli Pembelian';
    this.titleService.setTitle(title);
    this.items.map(value => this.isChecked[value.variant_id] = false);

    // check & updateProfile view, whether the user is back again from checkout page to cart.
    this.anyConfirmedItems();

    if (this.selectedItems.length === 0) {
      this.selectedItems = Object.assign([], this.items);
      this.items.map(value => this.isChecked[value.variant_id] = true);
      this.updateGrandTotal();
      this.isCheckAll = true;
    }
  }

  deleteFromCart(variant_id: number, cartId: number): void {
    this.cartService.delete(variant_id, cartId);
    this.selectedItems.splice(
      this.selectedItems.findIndex(_item => _item.variant_id === variant_id), 1);
    this.updateGrandTotal();
  }

  decreaseItemQty(item: CartItem): void {
    if (item.quantity > 1) {
      this.cartService.decrease(item);
    }

    this.onChangeSelectedItemQuantity(item);
    this.updateGrandTotal();
  }

  increaseItemQty(item: CartItem): void {
    if (item.quantity < item.variant_stock) {
      this.cartService.increase(item);
    }

    this.onChangeSelectedItemQuantity(item);
    this.updateGrandTotal();
  }

  onChangeSelectedItemQuantity(item: CartItem) {
    const i = this.selectedItems.findIndex(p => p.variant_id === item.variant_id);
    if (i > -1) {
      this.selectedItems[i].quantity = item.quantity;
    }
  }

  selectItem(item): void {

    if (this.isChecked[item.variant_id]) {

      this.selectedItems.push(item);

    } else {
      const i = this.selectedItems.findIndex((item_: CartItem) => item_.variant_id === item.variant_id);

      if (i > -1) {
        this.selectedItems.splice(i, 1);
      }
    }

    this.selectedItems.length === this.items.length ? this.isCheckAll = true : this.isCheckAll = false ;

    this.updateGrandTotal();
  }

  countCartGrandTotal(): number {
    return this.selectedItems
      .map(_item => _item.price * _item.quantity)
      .reduce((sum, i) => sum + i);
  }

  updateGrandTotal(): void {
    this.selectedItems.length > 0 ? this.cartGrandTotal = this.countCartGrandTotal() : this.cartGrandTotal = 0;
  }

  checkAll(): void {

    if (this.selectedItems.length < this.items.length) {
      this.items.map(item_ => {
        const i = this.selectedItems.findIndex((_item: CartItem) => _item.variant_id === item_.variant_id);

        if (item_.variant_stock > 0) {
          this.isChecked[item_.variant_id] = true;
          if (i === -1) {
            this.selectedItems.push(item_);
          }
        }
      });
    } else {
      this.selectedItems.map(item_ => this.isChecked[item_.variant_id] = false);
      this.selectedItems = [];
    }

    this.isCheckAll ? this.isCheckAll = false : this.isCheckAll = true;

    this.updateGrandTotal();
  }

  uncheckAll(): void {
    this.isCheckAll = false;

    this.selectedItems.map(item_ => this.isChecked[item_.variant_id] = false);
    this.selectedItems = [];
    this.updateGrandTotal();
  }

  confirmCheckout(): void {
    this.cartService.clearConfirmed();
    this.cartService.setConfirmedItems(this.selectedItems);
    this.router.navigate(['cart/confirm']);
  }

  anyConfirmedItems(): void {
    this.selectedItems = this.cartService.confirmedItems;
    this.cartService.clearConfirmed();

    this.selectedItems.map(item => {
      if (item.variant_stock > 0) {
        this.isChecked[item.variant_id] = true;
      } else {
        this.selectedItems.splice(this.selectedItems.indexOf(item), 1);
      }
    });

    this.selectedItems.length === this.items.length ? this.isCheckAll = true : this.isCheckAll = false;

    this.updateGrandTotal();
  }

  goToProduct(product_id: number) {
    this.productService.getProductsByIdList([product_id])
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe(
        (response: any) => {
          this.clickedProduct = response.data[0];
        },
        () => {},
        () => {
          this.router.navigate(
            ['/', this.gender,
              this.clickedProduct.sub_department_slug,
              this.clickedProduct.kelompok_slug,
              this.clickedProduct.product_slug]);
        });
  }

  ngOnDestroy(): void {
  }
}
