import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CartStikyHeaderComponent } from './cart-stiky-header.component';

describe('CartStikyHeaderComponent', () => {
  let component: CartStikyHeaderComponent;
  let fixture: ComponentFixture<CartStikyHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartStikyHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartStikyHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
