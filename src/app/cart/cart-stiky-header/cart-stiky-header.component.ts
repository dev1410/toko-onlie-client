import {Component, OnDestroy, OnInit} from '@angular/core';
import {CartService} from '../cart.service';

@Component({
  selector: 'app-cart-stiky-header',
  templateUrl: './cart-stiky-header.component.html',
  styleUrls: ['./cart-stiky-header.component.css']
})
export class CartStikyHeaderComponent implements OnInit, OnDestroy {

  constructor(public cartService: CartService) { }

  ngOnInit() {
  }

  ngOnDestroy(): void {
  }

}
