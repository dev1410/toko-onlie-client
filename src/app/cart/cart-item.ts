export class CartItem {
  id: number;
  product_id: number;
  product_image: string;
  product_name: string;
  product_brand: string;
  product_gender: string;
  product_subcategory: string;
  product_group: string;
  product_slug: string;
  variant_id: number;
  variant_text: string;
  variant_image: string;
  variant_stock: number;
  price: number;
  discount: boolean;
  discount_price: number;
  quantity: number;
  weight: number;
  out_of_stock: boolean;
}
