export class Bank {
  id: number;
  name: string;
  rekening_name: string;
  rekening_number: string;
  is_virtual: boolean;
  description: string;
  logo_bank: string;
  type_bayar: number;
}
