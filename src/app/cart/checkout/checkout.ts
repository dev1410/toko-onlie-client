export class Checkout {
  shopping_cart_id: number[];
  type_bayar: number;
  address_id: number;
  expedition_name: string;
  expedition_cost: number;
  to_bank_id: number;
  bank_name: string;
  rekening_name: string;
  rekening_no: string;
  transaction_url: string;
}
