import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Bank} from './bank';
import {Observable} from 'rxjs';
import {Checkout} from './checkout';
import {ShippingOption} from './shipping-option';

const httpOptions = {
  headers: new HttpHeaders({
    'Accept':  'application/json',
  })
};

@Injectable({
  providedIn: 'root'
})
export class CheckoutService {
  API_URL = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getBanks(): Observable<Bank[]> {
    const endpoint = `${this.API_URL}/master/bank`;
    return this.http.get<Bank[]>(endpoint, httpOptions);
  }

  getCouriers(address_id: number, weight: number): Observable<ShippingOption[]> {
    const endpoint = `${this.API_URL}/calculate/expedition`;
    return this.http.post<ShippingOption[]>(endpoint, {address_id: address_id, weight: weight}, httpOptions);
  }

  checkout(checkout: Checkout) {
    const endpoint = `${this.API_URL}/order/checkout`;
    return this.http.post(endpoint, checkout);
  }
}
