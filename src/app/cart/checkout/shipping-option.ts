export class ShippingOption {
  code: string;
  name: string;
  service: string;
  description: string;
  value: number;
  etd: string;
  note: string;
}
