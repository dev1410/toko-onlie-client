import {Component, OnDestroy, OnInit} from '@angular/core';
import {CartItem} from '../cart-item';
import {Location} from '@angular/common';
import {Bank} from './bank';
import {CheckoutService} from './checkout.service';
import {Address} from '../../shared/models/address';
import {AddressService} from '../../shared/services/address.service';
import {FormControl, Validators} from '@angular/forms';
import {AddressSuggestion} from '../../shared/models/address-suggestion';
import {Checkout} from './checkout';
import {Router} from '@angular/router';
import {ShippingOption} from './shipping-option';
import {Observable} from 'rxjs';
import {CartService} from '../cart.service';
import {takeUntil} from 'rxjs/operators';
import {componentDestroyed} from '@w11k/ngx-componentdestroyed';
import {Productservice} from '../../products/services/product.service';

declare var $: any;

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit, OnDestroy {
  isCheckoutSuccess: boolean;
  items: CartItem[];
  total: number;
  shipping_cost: number;
  cartTotal: number;
  public modalTitle: string;
  public paymentListSelected = false;
  weightTotal: number;
  addresses: Address[];
  selectedAddress: Address;
  selectedShipmentOption: ShippingOption;
  loading: boolean;

  checkoutFail = false;
  paymentListSelectedBank: Bank;
  addressSubscription;

  addressLabel = new FormControl('', [Validators.required, Validators.minLength(3)]);
  addressReceiver = new FormControl('', [Validators.required, Validators.minLength(3)]);
  addressDistrict = new FormControl('', [Validators.required]);
  addressHp = new FormControl('', [Validators.required, Validators.minLength(8)]);
  addressPostalCode = new FormControl('', [
    Validators.required,
    Validators.minLength(5),
    Validators.maxLength(5)]);
  addressText = new FormControl('', [Validators.required, Validators.minLength(8)]);

  bankAccountNumber = new FormControl('', [Validators.required, Validators.minLength(8)]);
  bankAccountName = new FormControl('', [Validators.required, Validators.minLength(2)]);

  shipments = new FormControl('Pilih Jasa Pengiriman');

  addressPrimary = false;
  isNewAddress: boolean;
  primaryAddress: Address;
  addressDistrictID: number;

  editedAddress: Address;
  isEditFromList: boolean;
  paymentType: number;

  banks$: Observable<Bank[]>;
  couriers$: Observable<ShippingOption[]>;
  addressSuggestions$: Observable<AddressSuggestion[]>;
  private clickedProduct: any;
  private gender = localStorage.getItem('__gdr') === '1' ? 'pria' : 'wanita';

  constructor(private _location: Location,
              private checkoutService: CheckoutService,
              private addressService: AddressService,
              private router: Router,
              private   cartService: CartService,
              private productService: Productservice) { }

  ngOnInit() {
    this.loading = true;
    this.items = JSON.parse(localStorage.getItem('confirmedItems'));
    this.total = this.items.map(item => item.price * item.quantity).reduce((sum, item) => sum + item);
    this.weightTotal = this.items.map(item => item.weight * item.quantity).reduce((sum, item) => sum + item);
    this.shipping_cost = 0;
    this.cartTotal = this.total + this.shipping_cost;

    this.addressService.list()
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe(
      (addresses: Address[]) => {
        const __primaryAddress = addresses.filter((address: Address) => address.is_primary)[0];

        this.selectedAddress = __primaryAddress;
        this.primaryAddress = __primaryAddress;

        this.addresses = addresses;
        if (__primaryAddress) {
          this.getCouriers(__primaryAddress);
        }
      },
          error => {},
        () => this.loading = false);

    this.shipments.valueChanges
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe((shippingOption: ShippingOption) => {
      this.selectedShipmentOption = shippingOption;
      this.shipping_cost = shippingOption.value;
    });
  }

  cancel(): void {

    if (!this.loading) {
      this.cartService.clearConfirmed();
      this.router.navigate(['/cart']);
    }
  }

  checkout(): void {
    this.banks$ = this.checkoutService.getBanks().pipe(takeUntil(componentDestroyed(this)));
    const headerWidth = $('#header').width();
    $('#modalBayar').modal({
      backdrop: 'static',
      keyboard: true,
      show : true
    });
    $('#header').width(headerWidth);
  }

  checkoutConfirm(): void {
    this.loading = true;
    const checkout = new Checkout;
    checkout.shopping_cart_id = this.items.map(item => item.id);
    if (checkout.shopping_cart_id && checkout.shopping_cart_id.length) {

      checkout.type_bayar = this.paymentType;
      checkout.address_id = this.selectedAddress.address_id;
      checkout.expedition_name = `${this.selectedShipmentOption.code} ${this.selectedShipmentOption.service}`;
      checkout.expedition_cost = this.selectedShipmentOption.value;
      checkout.to_bank_id = this.paymentListSelectedBank.id;
      checkout.bank_name = this.paymentListSelectedBank.name;
      // checkout.rekening_name = this.bankAccountName.value;
      // checkout.rekening_no = this.bankAccountNumber.value;
      checkout.transaction_url = `${location.hostname}/pengguna/transactions/invoice`;

      this.checkoutService.checkout(checkout)
        .pipe(takeUntil(componentDestroyed(this)))
        .subscribe(
          (next: any) => {
            if (next.code === 1) {
              this.isCheckoutSuccess = true;
              localStorage.setItem('checkoutSuccessData', JSON.stringify(next));
            } else if (next.code === 3) {
              this.checkoutFail = true;
              next.product_details.map(_ => {
                const i = this.cartService.items.findIndex((_item: CartItem) => _item.variant_id === _.product_variant_id);
                this.cartService.items[i].variant_stock = 0;

                const ii = this.cartService.confirmedItems.findIndex((_item: CartItem) => _item.variant_id === _.product_variant_id);
                this.cartService.confirmedItems[ii].variant_stock = 0;
              });
            }
          },
          error => {},
          () => {
            if (this.isCheckoutSuccess) {
              this.items = [];
              this.loading = false;
              $('#modalBayar').modal('hide');
              this.router.navigate(['/cart/purchased']);
              this.cartService.clear();
            }

            if (this.checkoutFail) {
              this.checkoutFailAction('Salah satu produk dalam keranjang sudah habis');
              this.router.navigate(['/cart']);
            }

            this.loading = false;
          });
    } else {
      this.loading = false;
    }
  }

  checkoutFailAction(message: string): void {
    this.loading = true;
    this.checkoutFail = true;
    this.cartService.clearConfirmed();
    this.cartService.setConfirmedItems(this.items);
    this.cartService.syncData();
    localStorage.setItem('checkoutFailMessage', message);
    this.loading = false;
  }

  editAddress(address: Address, fromList: boolean = false): void {
    this.modalTitle = 'Ubah Alamat';
    const headerWidth = $('#header').width();
    $('#modalChangeAddress').modal('hide');
    $('#modalAddress').modal({
      backdrop: 'static',
      keyboard: true,
      show : true
    });
    $('#header').width(headerWidth);

    this.editedAddress = address;
    if (fromList) {
      this.isEditFromList = true;
    }

    this.addressLabel.setValue(address.address_name);
    this.addressReceiver.setValue(address.receiver_name);
    this.addressHp.setValue(address.hp);
    this.addressText.setValue(address.address);
    this.addressPostalCode.setValue(address.postal_code);
    this.addressPrimary = address.is_primary;
    this.addressDistrictID = address.district_id;
    this.addressDistrict.setValue(`${address.province_name}, ` +
      `${address.city_name}, ${address.district_name}`);

    this.addressDistrict.valueChanges.subscribe(value => {
      if (value) {
        if (value.length > 3) {
          if (value.length < 9) {
            $('.autocomplete-items').show();
            this.addressSuggestions$ = this.addressService.getSuggestion(value);
          }
        } else {
          $('.autocomplete-items').hide();
        }
      }
    });
  }

  addAddress(): void {
    this.modalTitle = 'Tambah Alamat Baru';
    this.isNewAddress = true;
    const headerWidth = $('#header').width();
    $('#modalChangeAddress').modal('hide');
    $('#modalAddress').modal({
      backdrop: 'static',
      keyboard: true,
      show : true
    });
    $('#header').width(headerWidth);

    this.addressDistrict.valueChanges.subscribe(
      (next: string) => {
        if (next) {
          if (next.length > 3) {
            if (next.length < 9) {
              $('.autocomplete-items').show();
              this.addressSuggestions$ = this.addressService.getSuggestion(next);
            }
          } else {
            $('.autocomplete-items').hide();
          }
        }
      });
  }

  saveAddress() {
    const address = this.editedAddress ? this.editedAddress : new Address;

    address.address_name = this.addressLabel.value.trim();
    address.receiver_name = this.addressReceiver.value.trim();
    address.hp = this.addressHp.value.trim();
    address.district_id = this.addressDistrictID;
    address.postal_code = this.addressPostalCode.value.trim();
    address.address = this.addressText.value.trim();
    address.is_primary = this.addressPrimary;

    if (this.isNewAddress) {
      this.addressService.add(address)
        .pipe(takeUntil(componentDestroyed(this)))
        .subscribe(
          (_address: any) => {
            if (!this.selectedAddress) {
              const address_ = _address;
              address_.address_id = _address.id;
              this.setSelectedAddress(address_);
            } else {
              this.selectedAddress = _address;
            }
            this.addresses.push(_address);
            if (address.is_primary) {
              this.exchangePrimary(address);
            }
          },
          error => error);
      $('#modalAddress').modal('hide');
      this.resetForm();

      this.isNewAddress = undefined;
    } else {
      address.address_id = this.selectedAddress.address_id;
      this.addressSubscription = this.addressService.update(address)
        .subscribe( (res: any) => {
          if ('code' in res) {
            // do something with fail update
          } else {
            address.address_id = res.id;
            address.address_name = res.address_name;
            address.receiver_name = res.receiver_name;
            address.hp = res.hp;
            address.district_id = res.district_id;
            address.postal_code = res.postal_code;
            address.address = res.address;
            address.is_primary = res.is_primary;
            address.province_name = res.province_name;
            address.city_name = res.city_name;
            address.district_name = res.district_name;
            if (this.isEditFromList) {
              const i = this.addresses.findIndex((_address: Address) => _address.address_id === address.address_id);
              this.addresses[i] = address;
            } else {
              this.setSelectedAddress(address);
            }
            this.editedAddress = undefined;
            $('#modalAddress').modal('hide');
            this.resetForm();
          }
          if (address.is_primary) {
            this.exchangePrimary(address);
          }
        });
    }
  }

  setSelectedAddress(address: Address) {
    this.selectedShipmentOption = undefined;
    this.selectedAddress = address;
    this.getCouriers(address);
  }

  setAddressAsPrimary(address: Address): void {
    if (this.addressSubscription) {
      this.addressSubscription.unsubscribe();
    }

    this.addressSubscription = this.addressService.setPrimary(address).subscribe((res: any) => {
      if (res.code === 1) {
        this.primaryAddress.is_primary = false;
        address.is_primary = true;

        this.primaryAddress = address;
      }
    });
  }

  setDistrictText(suggestion: AddressSuggestion): void {
    $('.autocomplete-items').hide();
    this.addressDistrict.setValue(suggestion.name_text);
    this.addressDistrictID = suggestion.district_id;
  }

  changeAddress(): void {
    const headerWidth = $('#header').width();
    $('#modalChangeAddress').modal({
      backdrop: 'static',
      keyboard: true,
      show : true
    });
    $('#header').width(headerWidth);
  }

  selectAddress(address: Address) {
    this.setSelectedAddress(address);
    $('#modalChangeAddress').modal('hide');
  }

  paymentListSelect(bank: Bank) {
    this.paymentListSelected = true;
    this.paymentListSelectedBank = bank;
    this.paymentType = bank.type_bayar;

    $('#home').hide();
    $('#box-transfer').show();
    $('#modal-bayar-close').hide();
    $('.btn-confirm-bayar').show();
    $('.btn-wait').hide();
    return false;
  }

  selectPayment() {
    this.paymentListSelected = false;
    $('#home').show();
    $('#box-transfer').hide();
    $('#modal-bayar-close').show();
    $('#modal-bayar-back').hide();
    $('.btn-confirm-bayar').hide();
    $('.btn-wait').hide();
    return false;
  }

  clickDetail() {
    $('#form_data').hide();
    $('#detail-product-order').show();
    $('.header-detail').show();
    $('.header-bayar').hide();
    $('#amount_summary').hide();
  }

  closeDetail() {
    $('#form_data').show();
    $('#detail-product-order').hide();
    $('.header-detail').hide();
    $('.header-bayar').show();
    $('#amount_summary').show();
    return false;
  }

  resetForm(): void {
    this.addressLabel.reset();
    this.addressReceiver.reset();
    this.addressHp.reset();
    this.addressDistrict.reset();
    this.addressPostalCode.reset();
    this.addressText.reset();
    this.addressPrimary = false;
  }

  getCouriers(address: Address): void {
    this.couriers$ = this.checkoutService.getCouriers(address.address_id, this.weightTotal)
      .pipe(takeUntil(componentDestroyed(this)));
  }

  searchAddress(value: string): void {
    if (value) {
      const regex = new RegExp(value, 'i');
      const data = $('.panel-address');
      data.each(function(a, b) {
        $(b).hide();
        const input = $('.list-address-select-box', b);
        const isi = input[0]['innerText'];
        if (isi.match(regex)) {
          $(b).show();
        }
      });
    } else {
      $('.panel-address').show();
    }
  }

  exchangePrimary(address: Address) {
    if (address.is_primary) {
      this.addresses.map((addr: Address) => {
        if (addr.is_primary && addr.address_id !== address.address_id) {
          addr.is_primary = false;
        }
      });
    }
  }

  ngOnDestroy(): void {
    if (this.addressSubscription) {
      this.addressSubscription.unsubscribe();
    }
  }

  goToProduct(product_id: number) {
    this.productService.getProductsByIdList([product_id])
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe(
        (response: any) => {
          this.clickedProduct = response.data[0];
        },
        () => {},
        () => {
          this.router.navigate(
            ['/', this.gender,
              this.clickedProduct.sub_department_slug,
              this.clickedProduct.kelompok_slug,
              this.clickedProduct.product_slug]);
        });
  }
}
