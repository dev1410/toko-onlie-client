import {Injectable} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RouterService {
  private genders = ['pria', 'wanita'];
  private path: string;
  private gender: string;

  constructor(private route: ActivatedRoute,
              private router: Router) {
    const gdr = localStorage.getItem('__gdr');
    if (gdr) {
      this.gender = gdr === '1' ? 'pria' : 'wanita';
    }
  }

  getGender() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {

        this.path = this.router.url;

        const paths = this.path.split('/');

        if (paths[1] === 'wanita' || paths[1] === 'pria') {
          this.setGender(paths[1]);
        }
      }
    });
    return this.gender;
  }

  setGender(gender: string) {
    if (this.genders.indexOf(gender) > -1) {
      const _gender = gender === 'wanita' ? '2' : '1';
      localStorage.setItem('__gdr', _gender);
      this.gender = gender;
    } else {
      return;
    }
  }

  getUrl(): string {
    return this.router.url;
  }

  getSubcategory() {
    return this.route.snapshot.paramMap.get('subcategory');
  }

  getGroup() {
    return this.route.snapshot.paramMap.get('group');
  }

  redirectToHome(): void {
    this.router.navigate(['/']);
  }

  goTo(path: string, queryParams: Object = null): void {
    queryParams ? this.router.navigate([path, queryParams]) : this.router.navigate([path]);
  }
}
