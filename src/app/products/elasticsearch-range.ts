export class ElasticsearchRange {
  gt: number|string;
  gte: number|string;
  lt: number|any;
  lte: number|any;
  format: string;
}
