import { Injectable } from '@angular/core';
import {ProductBoxPrototype} from '../product-box-prototype';
import {PRODUCTSBOX} from '../mock-product-boxes';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {catchError} from 'rxjs/operators';
import {Observable, of} from 'rxjs';
import {ProductFilter} from '../ProductFilter';
import {Product} from '../product';
import {ProductImage} from '../product-image';
import {ProductVariant} from '../product-variant';
import {ProductTile} from '../product-tile';
import {Subdepartment} from '../../header/navbar/subdepartment';
import {Subkelompok} from '../subkelompok';

const ENDPOINT = environment.apiUrl;

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class Productservice {
  productsFilterUrl = `${ENDPOINT}/product/tiles/filter/flexible`;

  constructor(private http: HttpClient) { }

  getProductsSlider(): ProductBoxPrototype[] {
    return PRODUCTSBOX;
  }

  getProductRecomendations(tag: string): Observable<Product[]> {
    const endpoint = `${ENDPOINT}/recommeded/product/data`;
    return this.http.post<Product[]>(endpoint, {'tag': tag}, httpOptions);
  }

  getProductsCategory1(): ProductBoxPrototype[] {
    return PRODUCTSBOX;
  }

  getProductsCategory2(): ProductBoxPrototype[] {
    return PRODUCTSBOX;
  }

  getProductsCategory3(): ProductBoxPrototype[] {
    return PRODUCTSBOX;
  }

  getProduct(slug: string): Observable<Product> {
    const getProductURL = `${ENDPOINT}/product/${slug}`;
    return this.http.get<Product>(getProductURL);
  }

  getImages(product_id): Observable<ProductImage[]> {
    const getImagesURL = `${ENDPOINT}/product/images`;
    return this.http.post<ProductImage[]>(getImagesURL, {'id': product_id}, httpOptions);
  }

  getVariants(product_id): Observable<ProductVariant[]> {
    const getVariantsURL = `${ENDPOINT}/product/variants`;
    return this.http.post<ProductVariant[]>(getVariantsURL, {'id': product_id}, httpOptions);
  }

  getProductsFilter(params: ProductFilter) {
    const body = JSON.stringify(params);
    return this.http.post<string>(this.productsFilterUrl, body, httpOptions)
      .pipe(
        catchError(this.errorHandler('products-filter'))
      );
  }

  getProductsFilterParams(params: ProductFilter) {
    const body = JSON.stringify(params);
    return this.http.post<string>(`${ENDPOINT}/product/tiles/filter/params/flexible`, body, httpOptions)
      .pipe(
        catchError(this.errorHandler('products-filter'))
      );
  }

  errorHandler<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // console.log(`${operation} failed: ${error.message} ${error.code}`);

      return of(result as T);
    };
  }

  getSubDepartmentBySlug(slug: string) {
    return this.http.get(`${ENDPOINT}/master/subdepartmens/get/${slug}`)
      .pipe(catchError(this.errorHandler('product-get-subdepartment')));
  }

  getSubDeptById(dept_id: number){
    return this.http.get(`${ENDPOINT}/master/sub/department/${dept_id}`)
      .pipe(catchError(this.errorHandler('product-get-subdepartment-id')));
  }

  getGroupById(subDept_id: number){
    return this.http.get(`${ENDPOINT}/master/kelompok/${subDept_id}`)
      .pipe(catchError(this.errorHandler('product-get-kelompok-id')));
  }

  getSearchSuggestions(query: string, gender: string): Observable<ProductTile[] | {}> {
    if (!query.length) {
      return;
    }
    query = query.replace(' ', '+').trim();
    return this.http.get<ProductTile[]>(`${ENDPOINT}/product/tiles/search/suggestion/flexible/${gender}/${query}`)
      .pipe(
        catchError(this.errorHandler('products-search-suggestion'))
      );
  }

  logView(product_id: number) {
    // this api require logged in user
    return this.http.post(`${ENDPOINT}/user/product/view/save`, {product_id: product_id}, httpOptions);
  }

  getSubdepartmentsByIdList(ids: number[]): Observable<Subdepartment[]> {
    return this.http.post<Subdepartment[]>(`${ENDPOINT}/master/subdepartments`, {ids: ids}, httpOptions);
  }

  getGroupsByIdList(ids: number[]): Observable<Subkelompok[]> {
    return this.http.post<Subkelompok[]>(`${ENDPOINT}/master/groups`, {ids: ids}, httpOptions);
  }

  getProductsByIdList(ids: number[]): Observable<ProductTile[]> {
    return this.http.post<ProductTile[]>(`${ENDPOINT}/product/tiles/filter/flexible`, {ids: ids}, httpOptions);
  }
}
