import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Productservice} from './product-service-inline';
import {ProductBoxPrototype} from '../product-box-prototype';
import {HttpWrapper} from '../../_services/http-wrapper';
import {takeUntil} from 'rxjs/operators';
import {componentDestroyed} from '@w11k/ngx-componentdestroyed';

var api: HttpWrapper;

@Component({
  selector: 'app-product-category',
  templateUrl: './product-category.component.html',
  styleUrls: ['./product-category.component.css']
})
export class ProductCategoryComponent implements OnInit, OnDestroy {

  categories: any[];

  sizeMiddle = 4;

  @Input()
  gender: string;

  constructor(private productService: Productservice) { }

  ngOnInit() {
      this.getRecommends();
  }

  getRecommends() {
      this.productService.getRecommends(this.gender)
        .pipe(takeUntil(componentDestroyed(this)))
        .subscribe((categories: ProductBoxPrototype[]) => {
          this.sizeMiddle = Math.round(12 / categories.length);
          this.categories = categories;
          this.categories.forEach((category: ProductBoxPrototype) => {
              category.product_recommended.forEach((product: any) => {
                  try {
                      product.product_variant.image = this.productService.wrapImage(product.product_variant.image);
                  } catch (e) {}
              });
          });
      });
  }

  ngOnDestroy(): void {
  }
}
