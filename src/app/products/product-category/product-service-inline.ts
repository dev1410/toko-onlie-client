import {HttpWrapper} from '../../_services/http-wrapper';
import {Injectable} from '@angular/core';
import {ProductBoxPrototype} from '../product-box-prototype';

var api: HttpWrapper;

@Injectable({
  providedIn: 'root'
})
export class Productservice {
    constructor(private http: HttpWrapper) {
        api = http;
    }

    getRecommends(gender: string) {
        return api.post<ProductBoxPrototype[]>("/recommeded/product/data", {tag: gender});
    }

    wrapImage(path: string): string {
        return api.wrapImage(path);
    }
}
