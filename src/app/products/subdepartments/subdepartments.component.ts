import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {Subdepartment} from '../../header/navbar/subdepartment';
import {Subkelompok} from '../subkelompok';
declare var $: any;

@Component({
  selector: 'app-subdepartments',
  templateUrl: './subdepartments.component.html',
  styleUrls: ['./subdepartments.component.css']
})
export class SubdepartmentsComponent implements OnChanges {
  active = [];
  activeChild = [];
  selectedSubdepartments = [];
  selectedGroups = [];
  @Input() subdepartments: Subdepartment[];
  @Input() include_groups: boolean;

  @Output() subdepartmentsFilter: EventEmitter<number[]> = new EventEmitter<number[]>(true);
  @Output() subkelompokFilter: EventEmitter<number[]> = new EventEmitter<number[]>(true);

  constructor() {}
  selectGroup(group: Subkelompok) {
    if (this.activeChild[group.id]) {
      this.selectedGroups.push(group.id);
      // @ts-ignore
      document.getElementById('__' + group.sub_department_id.toString()).indeterminate = true;
    } else {
      const i = this.selectedGroups.findIndex((k: number) => k === group.id);
      const j = this.subdepartments.findIndex(k => k.id === group.sub_department_id);

      if (i > -1) {
        this.selectedGroups.splice(i, 1);
      }

      if (j > -1) {
        let isEmptyChecked = true;
        this.subdepartments[j].groups.map(v => {
          if (this.selectedGroups.indexOf(v.id) > -1) {
            isEmptyChecked = false;
          }
        });
        if (isEmptyChecked) {
          // @ts-ignore
          document.getElementById('__' + group.sub_department_id.toString()).indeterminate = false;
          // @ts-ignore
          document.getElementById('__' + group.sub_department_id.toString()).checked = false;
        }
      }
    }
    this.subkelompokFilter.emit(this.selectedGroups);
  }

  selectSubdepartment(subdepartment: Subdepartment) {
    if (this.active[subdepartment.id]) {
      this.selectedSubdepartments.push(subdepartment.id);
    } else {
      const i = this.selectedSubdepartments.findIndex((k: number) => k === subdepartment.id);
      if (i > -1) {
        this.selectedSubdepartments.splice(i, 1);
      }
    }
    this.subdepartmentsFilter.emit(this.selectedSubdepartments);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.subdepartments) {
      $('.btn-filter-selected').remove();

      this.selectedSubdepartments = [];
      this.selectedGroups = [];
      this.subdepartmentsFilter.emit(this.selectedSubdepartments);
      this.subkelompokFilter.emit(this.selectedGroups);
      this.active = [];
      this.activeChild = [];
    }
  }
}
