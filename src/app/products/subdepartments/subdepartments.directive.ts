import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appSubdepartments]'
})
export class SubdepartmentsDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
