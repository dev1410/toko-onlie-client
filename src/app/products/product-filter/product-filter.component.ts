import {Component, ComponentFactoryResolver, EventEmitter, OnDestroy, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {RouterService} from '../../router.service';
import {Productservice} from '../services/product.service';
import {ActivatedRoute, Router} from '@angular/router';
import {BreadcrumbDirective} from '../../breadcrumb/breadcrumb.directive';
import {Breadcrumb} from '../../breadcrumb/breadcrumb';
import {BreadcrumbComponent} from '../../breadcrumb/breadcrumb.component';
import {Title} from '@angular/platform-browser';
import {SlugToTitleCasePipe} from '../../shared/pipes/slug-to-title-case.pipe';
import {FormControl} from '@angular/forms';
import {Subdepartment} from '../../header/navbar/subdepartment';
import {ProductFilter} from '../ProductFilter';
import {DepartmentService} from '../../shared/services/department.service';
import {takeUntil} from 'rxjs/operators';
import {componentDestroyed} from '@w11k/ngx-componentdestroyed';
import {ProductTilesDirective} from '../product-tile/product-tiles.directive';
import {ProductTilesComponent} from '../product-tile/product-tiles.component';
import {ProductTile} from '../product-tile';
import {PriceRangeDirective} from '../price-range/price-range.directive';
import {PriceRangeComponent} from '../price-range/price-range.component';
import {Subkelompok} from '../subkelompok';
import {ElasticsearchRange} from '../elasticsearch-range';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-product-filter',
  templateUrl: './product-filter.component.html',
  styleUrls: ['./product-filter.component.css'],
  styles: [],
})
export class ProductFilterComponent implements OnInit, OnDestroy {
  gender: string;
  subcategory: string;
  group: string;
  params: any;
  products: ProductTile[];
  subdepartment: Subdepartment;
  filter: ProductFilter = new ProductFilter();
  filterChanged: EventEmitter<ProductFilter> = new EventEmitter<ProductFilter>(true);
  categoryImage$: EventEmitter<string> = new EventEmitter<string>(true);

  brandOptions: string[];

  colorOptions: string[];

  sizeOptions: string[];

  priceRangeOptions: string;

  sortOptions = [
    {name: 'Produk Baru', value: 'newest'},
    {name: 'Harga Tertinggi', value: 'expensive'},
    {name: 'Harga Terendah', value: 'cheap'},
    {name: 'Harga Diskon', value: 'discount'},
  ];
  selectedSort = this.sortOptions[0];
  sortControl = new FormControl(this.selectedSort);

  totalPages = 1;
  pageSize = 30;
  current_page = 1;

  groups: Subkelompok[];

  @ViewChild(BreadcrumbDirective) breadcrumbDirective: BreadcrumbDirective;
  @ViewChild(ProductTilesDirective) productTilesDirective: ProductTilesDirective;
  @ViewChild(PriceRangeDirective) priceRangeDirective: PriceRangeDirective;

  private max_price_range: number;

  private timeout = null;
  private subDeptSubscription;
  private getProductsSubscription: Subscription;
  private getFilterParamsSubscription: Subscription;
  private getProductsTimeout = null;

  constructor(private routerService: RouterService,
              private productService: Productservice,
              private route: ActivatedRoute,
              private router: Router,
              private componentFactoryResolver: ComponentFactoryResolver,
              private viewContainerRef: ViewContainerRef,
              private titleService: Title,
              private deptService: DepartmentService) {
    this.params = {
      category: [3],
      subcategory: [5]
    };
  }

  ngOnInit() {
    this.gender = localStorage.getItem('__gdr') === '1' ? 'pria' : 'wanita';
    this.filter.gender = this.gender;
    this.filter.size = this.pageSize;
    this.filterChanged.emit(this.filter);

    this.route.params.forEach(params => {
      this.subcategory = params['subcategory'];
      this.group = params['group'];

      this.titleService.setTitle(
        `${new SlugToTitleCasePipe().transform(this.subcategory)}
        ${new SlugToTitleCasePipe().transform(this.gender)}`);
      this.getSubdepartment();
      this.loadBreadcrumb();
    });

    this.filterChanged
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe((v: ProductFilter) => {
        this.getProducts(v);
      });

    this.sortControl.valueChanges
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe((value_: any) => {
        this.resetPage();
        this.filter.sort = value_.value;
        this.filterChanged.emit(this.filter);
      });
  }

  getFilterParams(filter: ProductFilter) {
    if (this.getFilterParamsSubscription) {
      this.getFilterParamsSubscription.unsubscribe();
    }

    this.getFilterParamsSubscription = this.productService.getProductsFilterParams(filter)
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe( (res: any) => {
        this.sizeOptions = res.sizes;
        this.brandOptions = res.brands;
        this.colorOptions = res.colors;
        this.max_price_range = res.max_price;
        this.groups = res.groups;
        if (res.max_price) {
          this.loadPriceFilter(this.max_price_range);
        } else {
          this.priceRangeDirective.viewContainerRef.clear();
        }
      });
  }

  ngOnDestroy(): void {
    this.viewContainerRef.clear();
    localStorage.removeItem('__sdt');
  }

  getSubdepartment(): void {
    if (this.subDeptSubscription) {
      this.subDeptSubscription.unsubscribe();
    }
    clearTimeout(this.timeout);
    this.timeout = setTimeout( () => {
      this.timeout = this.deptService.getSubdepartment(this.subcategory)
        .pipe(takeUntil(componentDestroyed(this)))
        .subscribe((resp: Subdepartment) => {
          this.subdepartment = resp;
          this.current_page = 1;
          this.filter = new ProductFilter();
          this.selectedSort = this.sortOptions[0];

          this.categoryImage$.emit(resp.image);
          this.filter.subcategory = [this.subdepartment.id];
          this.filter.gender = this.gender;
          this.filter.size = this.pageSize;
          this.filter.sort = this.sortOptions[0].value;
          this.filterChanged.emit(this.filter);
          this.getFilterParams(this.filter);
        });
    }, 500);
  }

  getProducts(filter: ProductFilter): void {
    if (this.getProductsSubscription) {
      this.getProductsSubscription.unsubscribe();
    }
    clearTimeout(this.getProductsTimeout);
    this.getProductsTimeout = setTimeout(() => {
      this.getProductsSubscription = this.productService.getProductsFilter(filter)
        .pipe(takeUntil(componentDestroyed(this)))
        .subscribe((value: any) => {
          if (value) {
            this.totalPages = Math.ceil(value.total / this.pageSize);
            this.products = value.data;
            this.loadProductTiles(this.products);
          }
        });
    }, 500);
  }

  loadBreadcrumb() {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(BreadcrumbComponent);
    const viewContainerRef = this.breadcrumbDirective.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent(componentFactory);

    const breadcrumb = (<Breadcrumb>componentRef.instance);
    breadcrumb.gender = this.gender;
    breadcrumb.subcategory = this.subcategory;

    if (this.group) {
      breadcrumb.group = this.group;
    }
  }

  loadPriceFilter(max_price: number): void {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(PriceRangeComponent);
    const viewContainerRef = this.priceRangeDirective.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent(componentFactory);
    const rangeFilter = (<PriceRangeComponent>componentRef.instance);
    rangeFilter.max_range = max_price;

    rangeFilter.priceRange
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe(value => {
        this.priceRangeOptions = value;
        const elasticsearchRange = new ElasticsearchRange();
        elasticsearchRange.gte = +value.split('-')[0];
        elasticsearchRange.lte = +value.split('-')[1];
        this.filter.price_range = elasticsearchRange;
        this.filterChanged.emit(this.filter);
      });
  }

  loadProductTiles(products: ProductTile[]): void {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(ProductTilesComponent);
    const viewContainerRef = this.productTilesDirective.viewContainerRef;
    viewContainerRef.clear();
    const componentRef = viewContainerRef.createComponent(componentFactory);
    const productTiles = (<ProductTilesComponent>componentRef.instance);
    productTiles.gender = this.gender;
    productTiles.products = products;
  }

  subkelompokFiltered(event) {
    this.resetPage();
    this.filter.group = event;
    this.filterChanged.emit(this.filter);
  }

  sizeFiltered(event) {
    this.resetPage();
    this.filter.variant_size = event.map(e => e.toLowerCase());
    this.filterChanged.emit(this.filter);
  }

  brandFiltered(event) {
    this.resetPage();
    this.filter.brand = event.map(e => e.toLowerCase());
    this.filterChanged.emit(this.filter);
  }

  colorFiltered(event) {
    this.resetPage();
    this.filter.variant_color = event.map(e => e.toLowerCase());
    this.filterChanged.emit(this.filter);
  }

  loadRequestedPage(event) {
    this.filter.from = (event - 1 ) * this.pageSize;
    this.current_page = event;
    this.filterChanged.emit(this.filter);
  }

  resetFilter(): void {
    this.current_page = 1;
    this.filter = new ProductFilter();
    this.filter.gender = this.gender;
    this.filter.subcategory = [this.subdepartment.id];
    this.filter.size = this.pageSize;
    this.selectedSort = this.sortOptions[0];
    this.filterChanged.emit(this.filter);
    this.getFilterParams(this.filter);
  }

  resetPage() {
    this.filter.from = undefined;
    this.current_page = 1;
  }
}
