import {ProductVariant} from './product-variant';
import {ProductImage} from './product-image';

export class Product {
  id: number;
  name: string;
  merk: string;
  brand: string;
  category_id: number;
  sub_category_id: number;
  sub_kelompok_id: number;
  description: string;
  review: string;
  weight: number;
  length: number;
  width: number;
  height: number;
  diameter: number;
  warranty: string;
  model: string;
  condition: string;
  type_product: number;
  product_origin: string;
  ingredients: string;
  package_completeness: string;
  rating: string;
  is_deleted: boolean;
  created_at: string;
  slug: string;
  tag: string;
  variant: ProductVariant;
  all_image: ProductImage;
}
