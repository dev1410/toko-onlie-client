import {Component, Input, OnDestroy, OnInit, Output, EventEmitter} from '@angular/core';
import {WishlistService} from '../../wishlist/wishlist.service';
import {HttpWrapper} from '../../_services/http-wrapper';
import { Router } from '@angular/router';
import {ProductTile} from '../../products/product-tile';
import { UserService } from 'src/app/user/user.service';
import { takeUntil } from 'rxjs/operators';
import { componentDestroyed } from '@w11k/ngx-componentdestroyed';
import { trigger, state, style, transition, animate } from '@angular/animations';
import {ProductBoxPrototype} from '../product-box-prototype';

var api:HttpWrapper;

@Component({
  selector: 'app-product-slider',
  templateUrl: './product-slider.component.html',
  styleUrls: ['./product-slider.component.css'],
  animations: [
    trigger('tgFavorite', [
      // ...
      state('unActive', style({
        color: 'green',
      })),
      state('actived', style({
        color: 'red'
      })),
      transition('actived <=> unActive', [
        animate('0.5s')
      ]),
    ]),
  ]
})
export class ProductSliderComponent implements OnInit, OnDestroy {
  @Input()
  gender: string;

 @Output() myEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  items = [];
  private listSubscribe;
  productsSlider: Array<ProductTile[]>;

  constructor(
    private userService: UserService,
    private wishlistService: WishlistService,
    private http: HttpWrapper,
    private router: Router) {
      api = http;
      this.gender = router.url.replace(/^[\/]+([^\/]+)/, '$1');
  }

  ngOnInit() {
    if (this.userService.isLoggedIn) {
      this.checkWishlist();
    }
  }

  @Input()
  set selectedTab(tab: string) {
      this.items = [];
      this.productsSlider = new Array<ProductTile[]>();
      if (tab) {
          let formData = new FormData();
          formData.append("tag", this.gender);
          formData.append("paginate", "10");
          formData.append("type", tab);
          api.post<any>("/slider/product", formData)
            .pipe(takeUntil(componentDestroyed(this)))
            .subscribe((res:any) => {

              // var tmp = {data: [], active: true};
              // this.items.push(tmp);
              // for (var i = 0; i < res.data.length; i++) {
              //     if ((i+1)%3 == 0 && i > 0) {
              //         tmp = {data: [], active: false};
              //         if (tmp.data.length) {
              //             this.items.push(tmp);
              //         }
              //     } else {
              //         let o = res.data[i];
              //         let tile = new ProductTile();
              //         for (var a in o) {
              //             try {
              //                 tile[a] = o[a];
              //             } catch (e) {}
              //         }
              //         tmp.data.push(tile);
              //     }
              // }
              //
              // for (var c in this.items) {
              //     this.productsSlider.push(this.items[c].data);
              // }

              const temp_array = [];
              let count = 0;
              let page = 1;
              const products = res.data.map(item => {
                item.tag = this.gender;
                return item;
              });

              while (count < res.data.length) {
                temp_array.push(this.paginate(products, 4, page));
                count += 4;
                page += 1;
              }
              this.productsSlider = temp_array;
          });
      }
  }

  checkWishlist(){
      if (this.userService.isLoggedIn()) {
        this.wishlistService.getAll()
        .pipe(takeUntil(componentDestroyed(this)))
        .subscribe((ress: any) => {
        if (ress.length > 0) {
          if (this.productsSlider && this.productsSlider[0] && this.productsSlider[0].length) {
            this.productsSlider.forEach(element => {
              element.map(temp => {
                const i = ress.findIndex(item => item === temp.product_id);
                if (i > -1) {
                  temp.isWishlist = true;
                } else {
                  temp.isWishlist = false;
                }
              });
            });
          }
        }
      });
    }
  }

  deleteWishlist(product_id: number) {
    this.wishlistService.delete(product_id)
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe((c: any) => {
        if(this.wishlistService.getOnDetail() === product_id){
          this.wishlistService.isWishlist = false;
        }
      });
  }


  tgButton(tempProduct: ProductTile) {
    if (this.userService.isLoggedIn()) {
        if(this.wishlistService.getOnDetail() === tempProduct.product_id){
          this.wishlistService.setToggle(tempProduct.isWishlist);
        }
        if (tempProduct.isWishlist) {
          this.toggles(tempProduct);
          this.deleteWishlist(tempProduct.product_id);
        } else {
          this.wishlistService.add(tempProduct.product_id)
          .pipe(takeUntil(componentDestroyed(this)))
          .subscribe((res: any) => {
            if(this.wishlistService.getOnDetail() === tempProduct.product_id){
              this.wishlistService.isWishlist = true;
            }
            this.toggles(tempProduct);
          });
        }
      } else {
        this.wishlistService.redirectToLoginPage();
      }
  }

  toggles(tempProduct: ProductTile){
    this.productsSlider[0].map(temp => {
      if(temp.product_id === tempProduct.product_id){
        temp.isWishlist = !tempProduct.isWishlist;
      }
    })
  }

  ngOnDestroy(): void {
    if (this.listSubscribe) {
      this.listSubscribe.unsubscribe();
    }
  }

  paginate(array, size, page): ProductBoxPrototype[] {
    --page;
    return array.slice(size * page, (page + 1) * size);
  }

}
