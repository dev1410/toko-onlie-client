import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appProductSlider]'
})
export class ProductSliderDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
