import {ElasticsearchRange} from './elasticsearch-range';

export class ProductFilter {
  name: string;
  price_range: ElasticsearchRange;
  variant_size: any[];
  variant_color: [];
  category: number[];
  subcategory: number[];
  group: number[];
  brand: string[];
  from: number;
  size: number;
  gender: string;
  sort: string;
}
