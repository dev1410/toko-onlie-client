export class ProductTile {
  product_id: number;
  product_name: string;
  product_slug: string;
  product_image: string;
  brand: string;
  price: number;
  promo_price: number;
  color: string;
  size: string;
  tag: string;
  department_id: number;
  department_name: string;
  department_slug: string;
  sub_department_id: string;
  sub_department_name: string;
  sub_department_slug: string;
  kelompok_id: string;
  kelompok_name: string;
  kelompok_slug: string;
  created_at: string;
  isWishlist: boolean;
  stok: number;
}
