import {Component, ComponentFactoryResolver, OnDestroy, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {ProductBoxPrototype} from '../product-box-prototype';
import {Productservice} from '../services/product.service';
import {BreadcrumbDirective} from '../../breadcrumb/breadcrumb.directive';
import {ActivatedRoute} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {RouterService} from '../../router.service';
import {BreadcrumbComponent} from '../../breadcrumb/breadcrumb.component';
import {Breadcrumb} from '../../breadcrumb/breadcrumb';
import {Product} from '../product';
import {Observable} from 'rxjs';
import {ProductImage} from '../product-image';
import {ProductVariant} from '../product-variant';
import {FormControl} from '@angular/forms';
import {CartService} from '../../cart/cart.service';
import {CartItem} from '../../cart/cart-item';
import {takeUntil} from 'rxjs/operators';
import {componentDestroyed} from '@w11k/ngx-componentdestroyed';
import {WishlistService} from '../../wishlist/wishlist.service';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {AddressService} from '../../shared/services/address.service';
import {AddressSuggestion} from '../../shared/models/address-suggestion';
import {ShippingOption} from '../../cart/checkout/shipping-option';
import {UserService} from '../../user/user.service';

import {ProductFilter} from '../ProductFilter';
import {Subdepartment} from '../../header/navbar/subdepartment';
import {ProductTile} from '../product-tile';
import {ProductSliderDirective} from '../product-slider/product-slider.directive';
import {ProductSliderComponent} from '../product-slider/product-slider.component';
import {WrapImagePipe} from '../../shared/pipes/wrap-image.pipe';

declare var $: any;

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css'],
  animations: [
    trigger('tgIcon', [
      // ...
      state('unActive', style({
        color: 'green',
      })),
      state('actived', style({
        color: 'red'
      })),
      transition('actived <=> unActive', [
        animate('0.5s')
      ]),
    ]),
  ]
})
export class ProductDetailComponent implements OnInit, OnDestroy {
  gender: string;
  subcategory: string;
  subdepartment: Subdepartment;
  group: string;
  product: string;
  product_item: Product;
  product_images$: Observable<ProductImage[]>;
  product_variants: Array<ProductVariant[]>;
  variants: ProductVariant[];
  mainImage: string;
  mainPrice = 0;
  discountPrice: number;
  selected_variant: ProductVariant = new ProductVariant();
  stockValue = new FormControl(1);
  isWishlist = false;
  tempData = [];

  shippingOptions: any[] = [];

  addressVillage = new FormControl('');

  private loading = false;

  productsSlider: Array<ProductTile[]>;

  @ViewChild(BreadcrumbDirective) breadcrumb: BreadcrumbDirective;
  private addressSuggestions$: Observable<AddressSuggestion[]>;

  @ViewChild(ProductSliderDirective) productSliderDirective: ProductSliderDirective;
  private addressSuggestionDistrict: AddressSuggestion;

  constructor(private productService: Productservice,
              private routerService: RouterService,
              private route: ActivatedRoute,
              private componentFactoryResolver: ComponentFactoryResolver,
              private viewContainerRef: ViewContainerRef,
              private titleService: Title,
              private cartService: CartService,
              private wishlistService: WishlistService,
              private addressService: AddressService,
              private userService: UserService) { }

  ngOnInit() {
    this.gender = this.routerService.getGender();
    this.subcategory = this.route.snapshot.paramMap.get('subcategory');
    this.group = this.route.snapshot.paramMap.get('group');
    this.wishlistService.setOnDetail(0);
    this.wishlistService.isWishlist = false;
    this.route.params.forEach(params => {
      const slug = params['product'];
      this.productService.getProduct(slug)
        .pipe(takeUntil(componentDestroyed(this)))
        .subscribe( (serverResponse: Product) => {
          if ('code' in serverResponse) {
            this.routerService.redirectToHome();
          }
          
          this.product_item = serverResponse;
          this.product = this.product_item.slug;
          this.titleService.setTitle(this.product_item.name);
          this.loadBreadcrumb();
          this.getVariants();
          this.getImages();
          this.productWishlist();
          
          if (this.userService.isLoggedIn()) {
            this.productService.logView(this.product_item.id)
              .pipe(takeUntil(componentDestroyed(this)))
              .subscribe( c => c);
          }
        });
    });

    $('input').iCheck({
      radioClass: 'iradio_minimal',
    });

    this.addressVillage.valueChanges
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe(value => {
      if (value) {
        if (value.length > 3) {
          if (value.length < 20) {
            $('.autocomplete-items').show();
            this.addressSuggestions$ = this.addressService.getSuggestion(value);
          }
        } else {
          $('.autocomplete-items').hide();
        }
      }
    });

    this.getSubdepartment();

    this.stockValue.valueChanges.subscribe(value => {
      if (this.addressSuggestions$ && this.addressSuggestionDistrict) {
        this.addressService.simulateShippingCost(
          this.addressSuggestionDistrict.district_id,
          this.product_item.weight * value).subscribe(
          (shippingOptions: ShippingOption[]) => {
            const temps_array = [];
            shippingOptions.map(item => {
              temps_array.push({
                service: `${item.code} ${item.service}`,
                etd: item.etd,
                cost: item.value
              });
            });
            this.shippingOptions = temps_array;
          }
        );
      }
    });
  }

  toggle() {
    this.wishlistService.isWishlist = !this.wishlistService.isWishlist;
  }

  increaseStockValue(): void {
    if (this.stockValue.value < this.selected_variant.stock) {
      this.stockValue.setValue(this.stockValue.value + 1);
    }
  }

  keepAtMaxStockValue() {
    if (this.stockValue.value > this.selected_variant.stock) {
      this.stockValue.setValue(this.selected_variant.stock);
    }

    if (this.stockValue.value === '' && this.selected_variant.stock > 0) {
      this.stockValue.setValue(1);
    }
  }

  decreaseStockValue(): void {
    if (this.stockValue.value > 1) {
      this.stockValue.setValue(this.stockValue.value - 1);
    }
  }

  setMainImage(img: string): void {
    this.mainImage = new WrapImagePipe().transform(img);
  }

  setVillageText(suggestion: AddressSuggestion): void {
    $('.autocomplete-items').hide();
    this.addressSuggestionDistrict = suggestion;
    this.addressService.simulateShippingCost(suggestion.district_id, this.product_item.weight * this.stockValue.value)
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe(
        (shippingOptions: ShippingOption[]) => {
          const temps_array = [];
          shippingOptions.map(item => {
            temps_array.push({
              service: `${item.code} ${item.service}`,
              etd: item.etd,
              cost: item.value
            });
          });
          this.shippingOptions = temps_array;
      });
    this.addressVillage.setValue(suggestion.name_text);
  }

  setVariant(variant: ProductVariant) {
    if (variant.image && variant.image !== 'null' && !variant.image.includes('product_default.png') && !variant.image.includes('null')) {
      this.mainImage = new WrapImagePipe().transform(variant.image);
    } else {
      this.product_images$.subscribe(c => {
        if (c && c.length) {
          this.mainImage = new WrapImagePipe().transform(c[0].image);
        }
      });
    }
    this.mainPrice = variant.price;
    this.selected_variant = variant;

    if (variant.discount) {
      this.discountPrice = variant.price;
      this.mainPrice = variant.promo_price;
    }
    variant.stock > 0 ? this.stockValue.setValue(1) : this.stockValue.setValue(0);
  }

  ngOnDestroy(): void {
    this.getVariants().unsubscribe();
  }

  getSubdepartment() {
    this.productService.getSubDepartmentBySlug(this.subcategory)
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe( (subcategory: Subdepartment) => {
        this.subdepartment = subcategory;
        this.getProductsSlider(subcategory);
      });
  }

  getProductsSlider(subdepartment: Subdepartment): void {
    const filter = new ProductFilter();
    filter.gender = this.gender;
    filter.subcategory = [subdepartment.id];
    filter.size = 12;


    this.productService.getProductsFilter(filter)
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe( (res: any) => {
        const temp_array = [];
        let count = 0;
        let page = 1;
        const products = res.data.map(item => {
          item.tag = this.gender;
          return item;
        });

        while (count < res.data.length) {
          temp_array.push(this.paginate(products, 4, page));
          count += 4;
          page += 1;
        }
        this.productsSlider = temp_array;
        this.loadProductSlider();
      });
  }

  paginate(array, size, page): ProductBoxPrototype[] {
    --page;
    return array.slice(size * page, (page + 1) * size);
  }

  getImages(): void {
    this.product_images$ = this.productService.getImages(this.product_item.id);
  }

  getVariants() {
    return this.productService.getVariants(this.product_item.id)
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe( (value: ProductVariant[]) => {
      const variantLength = value.length;

      const temp_array = [];
      let count = 0;
      let page = 1;

      while (count < variantLength) {
        temp_array.push(this.paginate(value, 4, page));
        count += 4;
        page += 1;
      }

      this.product_variants = temp_array;
      this.variants = value;

      // set default product view to the first variant
      if (this.product_variants.length > 0) {
        this.setVariant(this.product_variants[0][0]);
      }
    });
  }

  loadBreadcrumb() {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(BreadcrumbComponent);
    const viewContainerRef = this.breadcrumb.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent(componentFactory);

    const breadcrumb = (<Breadcrumb>componentRef.instance);
    breadcrumb.gender = this.gender;
    breadcrumb.subcategory = this.subcategory;
    breadcrumb.group = this.group;
    breadcrumb.product = this.product;
    breadcrumb.product_name = this.product_item.name;

    if (this.group) {
      breadcrumb.group = this.group;
    }
  }

  addToCart(): void {
    this.loading = true;
    const cartItem = new CartItem;

    cartItem.quantity = this.stockValue.value;
    cartItem.product_id = this.product_item.id;
    cartItem.product_slug = this.product_item.slug;
    this.product_images$
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe((value: ProductImage[]) => cartItem.product_image = value[0].image);
    cartItem.product_name = this.product_item.name;
    cartItem.product_brand = this.product_item.brand;
    cartItem.product_subcategory = this.subcategory;
    cartItem.product_gender = this.gender;
    cartItem.product_group = this.group;

    cartItem.variant_id = this.selected_variant.id;
    cartItem.variant_image = this.selected_variant.image;
    cartItem.discount = this.selected_variant.discount !== 0;
    cartItem.discount_price = cartItem.discount ? this.selected_variant.price : undefined;
    cartItem.price = this.selected_variant.discount ? this.selected_variant.promo_price : this.selected_variant.price;
    cartItem.variant_stock = this.selected_variant.stock;
    cartItem.variant_text = this.selected_variant.variant_text;
    cartItem.weight = this.product_item.weight;

    this.cartService.add(cartItem);
    this.loading = false;
  }

  addToWishlist(): void {
    
    if (this.userService.isLoggedIn()) {
      this.loading = true;
      if (this.wishlistService.isWishlist) {
        this.deleteWishlist();
      } else {
        this.tempData = [];
        this.wishlistService.add(this.product_item.id)
        .pipe(takeUntil(componentDestroyed(this)))
        .subscribe((httpResponse: any) => {
          setTimeout(() => {
            this.getSubdepartment();
            // this.productWishlist();
            this.loadProductSlider();
          }, 200);
        });
      }
      this.toggle();
      this.loading = false;
    } else {
      this.wishlistService.redirectToLoginPage();
    }
  }

  deleteWishlist() {
    this.tempData = [];
    this.wishlistService.delete(this.product_item.id)
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe((c: any) => {
        setTimeout(() => {
          this.getSubdepartment();
          // this.productWishlist();
          this.loadProductSlider();
        }, 300);
      });

  }
  productWishlist() {
    if (this.userService.isLoggedIn()) {
      this.wishlistService.getAll()
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe((ress: any) => {
        const i = ress.findIndex(item => item === this.product_item.id);
        if (i > -1 ) {
          this.wishlistService.setOnDetail(this.product_item.id);
          this.wishlistService.isWishlist = true;
        } else {
          this.wishlistService.setOnDetail(this.product_item.id);
          this.wishlistService.isWishlist = false;
        }

        this.loadProductSlider();
      });

    }
  }

  loadProductSlider() {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(ProductSliderComponent);
    const viewContainerRef = this.productSliderDirective.viewContainerRef;
    viewContainerRef.clear();
    const componentRef = viewContainerRef.createComponent(componentFactory);
    const productSlider = (<ProductSliderComponent>componentRef.instance);
    productSlider.productsSlider = this.productsSlider;
  }

}
