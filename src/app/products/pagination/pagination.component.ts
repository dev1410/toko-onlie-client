import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent {
  @Input() totalPages: number;
  @Input() current_page: number;

  @Output() requestedPage: EventEmitter<number> = new EventEmitter<number>(true);

  loadPage(page_number: number) {
    this.current_page = page_number;
    this.requestedPage.emit(page_number);
  }

  calculatePages(totalPages: number): number[] {
    const arr = [];
    for (let i = 1; i <= totalPages; i++) {
      arr.push(i);
    }
    return arr;
  }
}
