import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appPagination]'
})
export class PaginationDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
