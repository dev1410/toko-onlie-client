import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {FormControl} from '@angular/forms';

declare var $: any;

@Component({
  selector: 'app-brand-options',
  templateUrl: './brand-options.component.html',
  styleUrls: ['./brand-options.component.css']
})
export class BrandOptionsComponent implements OnChanges {
  active = [];
  selectedBrands = [];
  @Input() brands = [];
  @Output() brandsFilter: EventEmitter<string[]> = new EventEmitter<string[]>(true);
  searchBrandControl = new FormControl('');

  selectBrand(brand: string, index) {
    if (this.active[index]) {
      this.selectedBrands.push(brand);
    } else {
      const i = this.selectedBrands.findIndex((k: string) => k === brand);
      if (i > -1) {
        this.selectedBrands.splice(i, 1);
      }
    }
    this.brandsFilter.emit(this.selectedBrands);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.brands) {
      $('.btn-filter-selected').remove();

      this.selectedBrands = [];
      this.brandsFilter.emit(this.selectedBrands);
      this.active = [];
    }
  }

  searchBrand(): void {
    if (this.searchBrandControl.value) {
      const regex = new RegExp(this.searchBrandControl.value, 'i');
      const data = $('.filter-brand-option label');

      data.each((a, b) => {
        $(b).hide();

        const input = $('input', b);
        const isi = input[0]['attributes']['data-value']['nodeValue'];

        if (isi.match(regex)) {
          $(b).show();
        }
      });
    } else {
      $('.filter-brand-option label').show();
    }
  }
}
