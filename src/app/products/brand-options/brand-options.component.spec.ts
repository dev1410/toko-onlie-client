import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrandOptionsComponent } from './brand-options.component';

describe('BrandOptionsComponent', () => {
  let component: BrandOptionsComponent;
  let fixture: ComponentFixture<BrandOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrandOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrandOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
