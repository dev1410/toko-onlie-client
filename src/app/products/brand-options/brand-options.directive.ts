import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appBrandOptions]'
})
export class BrandOptionsDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
