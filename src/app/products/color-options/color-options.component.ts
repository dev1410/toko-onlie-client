import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-color-options',
  templateUrl: './color-options.component.html',
  styleUrls: ['./color-options.component.css']
})
export class ColorOptionsComponent implements OnChanges {
  active = [];
  selectedColors = [];
  @Input() colors = [];
  @Output() colorsFilter: EventEmitter<string[]> = new EventEmitter<string[]>(true);

  selectColor(color: string, index) {
    if (this.active[index]) {
      this.selectedColors.push(color);
    } else {
      const i = this.selectedColors.findIndex((k: string) => k === color);
      if (i > -1) {
        this.selectedColors.splice(i, 1);
      }
    }
    this.colorsFilter.emit(this.selectedColors);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.colors) {
      $('.btn-filter-selected').remove();

      this.selectedColors = [];
      this.colorsFilter.emit(this.selectedColors);
      this.active = [];
    }
  }
}
