import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appColorOptions]'
})
export class ColorOptionsDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
