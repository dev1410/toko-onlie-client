export class ProductVariant {
  id: number;
  product_id: number;
  image: string;
  code: number;
  barcode: number;
  sku_number: number;
  price: number;
  stock: number;
  min_stock: number;
  max_stock: number;
  discount: number;
  promo_discount: null;
  promo_syarat: null;
  promo_price: number;
  description: null;
  is_deleted: false;
  variant_text: string;
}
