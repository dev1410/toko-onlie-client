import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appProductTiles]'
})
export class ProductTilesDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}
