import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ProductTile} from '../product-tile';
import { WishlistService } from '../../wishlist/wishlist.service';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { UserService } from 'src/app/user/user.service';
import { takeUntil } from 'rxjs/operators';
import { componentDestroyed } from '@w11k/ngx-componentdestroyed';

@Component({
  selector: 'app-product-tiles',
  templateUrl: './product-tiles.component.html',
  styleUrls: ['./product-tiles.component.css'],
  animations: [
    trigger('tgFavorite', [
      // ...
      state('unActive', style({
        color: 'green',
      })),
      state('actived', style({
        color: 'red'
      })),
      transition('actived <=> unActive', [
        animate('0.5s')
      ]),
    ]),
  ]
})
export class ProductTilesComponent implements OnInit, OnDestroy {
  @Input() gender: string;
  @Input() products: ProductTile[];
  private listSubscribe;
  private userLogin: boolean;
  tempProductTile= [];
  constructor (
    private wishlistService: WishlistService,
    private userService: UserService) {}

  ngOnInit() {
    if (this.userService.isLoggedIn()) {
      this.checkWishlist();
      this.userLogin = true;
    } else {
      this.userLogin = false;
    }
    // this.checkLine();
  }

  deleteWishlist(product_id: number) {
    this.wishlistService.delete(product_id)
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe(c => c);
  }

  tgButton(tempProduct: ProductTile): void {
    if (this.userService.isLoggedIn()) {
      if (tempProduct.isWishlist) {
        this.toggles(tempProduct);
        this.deleteWishlist(tempProduct.product_id);
      } else {
        this.wishlistService.add(tempProduct.product_id)
        .pipe(takeUntil(componentDestroyed(this)))
        .subscribe((res: any) => {
          this.toggles(tempProduct);
        });
      }
    } else {
      this.wishlistService.redirectToLoginPage();
    }
    
  }

  toggles(tempProduct: ProductTile){
    this.products.map(temp => {
      if(temp === tempProduct){
        temp.isWishlist = !tempProduct.isWishlist;
      }
    })
  }

  checkWishlist() {
    if (this.userService.isLoggedIn()) {
      this.wishlistService.getAll()
        .pipe(takeUntil(componentDestroyed(this)))
        .subscribe((ress: any) => {
        if (this.products) {
          this.products.map(temp => {
            const i = ress.findIndex(item => item === temp.product_id);
            if (i > -1) {
              temp.isWishlist = true;
            } else {
              temp.isWishlist = false;
            }
          });
        }
      });
    }
  }
  loggedIn(){
    if(this.userLogin){
      return true;
    }else{
      return false;
    }
  }

  ngOnDestroy(): void {
    if (this.listSubscribe) {
      this.listSubscribe.unsubscribe();
    }
  }
}
