export class Subkelompok {
  id: number;
  sub_department_id: number;
  name: string;
  image: string;
  code: string;
  is_deleted: boolean;
  slug: string;
  tag: string;
}
