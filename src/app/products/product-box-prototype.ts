import {Product} from './product';

export class ProductBoxPrototype {
  id: number;
  variant_id: number;
  image: string;
  name: string;
  price: number;
  is_liked: number;
  url: string;
  kondisi: string;
  brand: string;
  tag: string;
  subcategory: string;
  subcategory_id: number;
  group: string;
  group_id: number;
  product_recommended: Product[];
}
