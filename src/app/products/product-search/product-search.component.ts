import {Component, ComponentFactoryResolver, EventEmitter, OnDestroy, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {ProductTile} from '../product-tile';
import {Subdepartment} from '../../header/navbar/subdepartment';
import {ProductFilter} from '../ProductFilter';
import {FormControl} from '@angular/forms';
import {ProductTilesDirective} from '../product-tile/product-tiles.directive';
import {PriceRangeDirective} from '../price-range/price-range.directive';
import {RouterService} from '../../router.service';
import {Productservice} from '../services/product.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {takeUntil} from 'rxjs/operators';
import {componentDestroyed} from '@w11k/ngx-componentdestroyed';
import {PriceRangeComponent} from '../price-range/price-range.component';
import {ProductTilesComponent} from '../product-tile/product-tiles.component';
import {ElasticsearchRange} from '../elasticsearch-range';
import {SubdepartmentsDirective} from '../subdepartments/subdepartments.directive';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-product-search',
  templateUrl: './product-search.component.html',
  styleUrls: ['./product-search.component.css']
})
export class ProductSearchComponent implements OnInit, OnDestroy {
  gender: string;
  subcategory: string;
  group: string;
  params: any;
  products: ProductTile[];
  subdepartment: Subdepartment;
  subdepartments: Subdepartment[];
  filter: ProductFilter = new ProductFilter();
  filterChanged: EventEmitter<ProductFilter> = new EventEmitter<ProductFilter>(true);
  categoryImage$: EventEmitter<string> = new EventEmitter<string>(true);

  brandOptions: string[];

  colorOptions: string[];

  sizeOptions: string[];

  priceRangeOptions: string;

  sortOptions = [
    {name: 'Produk Baru', value: 'newest'},
    {name: 'Harga Tertinggi', value: 'expensive'},
    {name: 'Harga Terendah', value: 'cheap'},
    {name: 'Harga Diskon', value: 'discount'},
  ];
  selectedSort = this.sortOptions[0];
  sortControl = new FormControl(this.selectedSort);
  max_price_range = 0;

  totalPages = 1;
  pageSize = 30;
  current_page = 1;

  @ViewChild(ProductTilesDirective) productTilesDirective: ProductTilesDirective;
  @ViewChild(PriceRangeDirective) priceRangeDirective: PriceRangeDirective;
  @ViewChild(SubdepartmentsDirective) subdepartmentsDirective: SubdepartmentsDirective;
  private nameQuery: string;
  private getFilterParamsSubscription: Subscription;
  private getProductsSubscription: Subscription;
  private getProductsTimeout = null;
  constructor(private routerService: RouterService,
              private productService: Productservice,
              private route: ActivatedRoute,
              private router: Router,
              private componentFactoryResolver: ComponentFactoryResolver,
              private viewContainerRef: ViewContainerRef,
              private titleService: Title) {
    this.params = {
      category: [3],
      subcategory: [5]
    };
  }

  ngOnInit() {
    this.gender = localStorage.getItem('__gdr') === '1' ? 'pria' : 'wanita';
    this.filter.gender = this.gender;
    this.filter.size = this.pageSize;
    this.filterChanged.emit(this.filter);

    this.route.queryParams
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe(params => {
        this.filter = new ProductFilter();
        this.filter.gender = this.gender;
        this.nameQuery = params['keyword'].replace('+', ' ');
        this.filter.name = this.nameQuery;
        this.filter.sort = this.sortOptions[0].value;
        this.filter.size = this.pageSize;
        this.getFilterParams(this.filter);
        this.getProducts(this.filter);
        this.titleService.setTitle(`Pencarian ${this.filter.name}`);
      });

    this.filterChanged
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe((v: ProductFilter) => {
        this.getProducts(v);
      });

    this.sortControl.valueChanges
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe((value_: any) => {
        this.resetPage();
        this.filter.sort = value_.value;
        this.filterChanged.emit(this.filter);
      });
  }

  ngOnDestroy(): void {
    this.viewContainerRef.clear();
    localStorage.removeItem('__sdt');
  }

  getFilterParams(filter: ProductFilter) {
    if (this.getFilterParamsSubscription) {
      this.getFilterParamsSubscription.unsubscribe();
    }
    this.getFilterParamsSubscription = this.productService.getProductsFilterParams(filter)
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe( (res: any) => {
        this.sizeOptions = res.sizes;
        this.brandOptions = res.brands;
        this.colorOptions = res.colors;
        this.max_price_range = res.max_price;
        this.subdepartments = res.subdepartments;
        if (this.max_price_range) {
          this.loadPriceFilter(res.max_price);
        } else {
          this.priceRangeDirective.viewContainerRef.clear();
        }
      });
  }

  getProducts(filter: ProductFilter): void {
    if (this.getProductsSubscription) {
      this.getProductsSubscription.unsubscribe();
    }
    clearTimeout(this.getProductsTimeout);
    this.getProductsTimeout = setTimeout(() => {
      this.getProductsSubscription = this.productService.getProductsFilter(filter)
        .pipe(takeUntil(componentDestroyed(this)))
        .subscribe((value: any) => {
          this.totalPages = Math.ceil(value.total / this.pageSize);
          this.products = value.data;
          this.loadProductTiles(this.products);
        });
    }, 500);
  }

  loadPriceFilter(max_price: number): void {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(PriceRangeComponent);
    const viewContainerRef = this.priceRangeDirective.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent(componentFactory);
    const rangeFilter = (<PriceRangeComponent>componentRef.instance);
    rangeFilter.max_range = max_price;

    rangeFilter.priceRange
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe(value => {
        this.priceRangeOptions = value;
        const elasticsearchRange = new ElasticsearchRange();
        elasticsearchRange.gte = +value.split('-')[0];
        elasticsearchRange.lte = +value.split('-')[1];
        this.filter.price_range = elasticsearchRange;
        this.filterChanged.emit(this.filter);
      });
  }

  loadProductTiles(products: ProductTile[]): void {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(ProductTilesComponent);
    const viewContainerRef = this.productTilesDirective.viewContainerRef;
    viewContainerRef.clear();
    const componentRef = viewContainerRef.createComponent(componentFactory);
    const productTiles = (<ProductTilesComponent>componentRef.instance);
    productTiles.gender = this.gender;
    productTiles.products = products;
  }

  subdepartmentFiltered(event) {
    this.filter.subcategory = event;
    this.resetPage();
    this.filterChanged.emit(this.filter);
  }

  subkelompokFiltered(event) {
    this.resetPage();
    this.filter.group = event;
    this.filterChanged.emit(this.filter);
  }

  sizeFiltered(event) {
    this.resetPage();
    this.filter.variant_size = event.map(e => e.toLowerCase());
    this.filterChanged.emit(this.filter);
  }

  brandFiltered(event) {
    this.resetPage();
     this.filter.brand = event.map(e => e.toLowerCase());
     this.filterChanged.emit(this.filter);
  }

  colorFiltered(event) {
    this.resetPage();
    this.filter.variant_color = event.map(e => e.toLowerCase());
    this.filterChanged.emit(this.filter);
  }

  loadRequestedPage(event) {
    this.filter.from = (event - 1 ) * this.pageSize;
    this.current_page = event;
    this.filterChanged.emit(this.filter);
  }

  resetFilter(): void {
    this.current_page = 1;
    this.filter = new ProductFilter();
    this.filter.gender = this.gender;
    this.filter.size = this.pageSize;
    this.filter.name = this.nameQuery;
    this.filter.sort = this.sortOptions[0].value;
    this.selectedSort = this.sortOptions[0];

    this.filterChanged.emit(this.filter);
    this.getFilterParams(this.filter);
  }

  resetPage() {
    this.filter.from = undefined;
    this.current_page = 1;
  }

}
