import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-size-options',
  templateUrl: './size-options.component.html',
  styleUrls: ['./size-options.component.css']
})
export class SizeOptionsComponent implements OnChanges {
  state = {};
  selectedSize = [];
  @Input() sizes = [];
  @Output() sizesFilter: EventEmitter<string[]> = new EventEmitter<string[]>(true);

  constructor() {
    this.sizes.map(size => {
      this.state[size] = false;
    });
  }

  changeState(index: number, size: string) {
    const i = this.selectedSize.findIndex(s => s === size);
    const exist = i > -1;

    if (this.state[size]) {
      if (exist) {
        this.selectedSize.splice(i, 1);
      }
      this.state[size] = false;
    } else {
      this.selectedSize.push(size);
      this.state[size] = true;
    }

    this.sizesFilter.emit(this.selectedSize);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.sizes) {
      $('.btn-filter-selected').remove();

      this.selectedSize = [];
      this.sizesFilter.emit(this.selectedSize);
      this.state = {};
    }
  }
}
