import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appSizeOptions]'
})
export class SizeOptionsDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
