import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-product-trending',
  templateUrl: './product-trending.component.html',
  styleUrls: ['./product-trending.component.css']
})
export class ProductTrendingComponent implements OnInit {

  @Input()
  gender: string;

  _selector: string;

  constructor() { }

  ngOnInit() {
    this._selector = 'newest';
  }
}
