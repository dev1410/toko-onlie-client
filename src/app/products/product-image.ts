export class ProductImage {
  id: number;
  product_id: number;
  image: string;
}
