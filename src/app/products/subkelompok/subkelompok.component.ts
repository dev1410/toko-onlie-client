import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {Subkelompok} from '../subkelompok';
import {Subdepartment} from '../../header/navbar/subdepartment';

declare var $: any;

@Component({
  selector: 'app-subkelompok',
  templateUrl: './subkelompok.component.html',
  styleUrls: ['./subkelompok.component.css']
})
export class SubkelompokComponent implements OnChanges {
  active = [];
  selectedKelompok = [];
  @Input() subdepartment: Subdepartment;
  @Input() subkelompok: Subkelompok;
  @Input() subkelompoks: Subkelompok[];

  @Output() groupsFilter: EventEmitter<number[]> = new EventEmitter<number[]>(true);

  selectKelompok(kelompok: Subkelompok) {
    if (this.active[this.subkelompoks.indexOf(kelompok)]) {
      this.selectedKelompok.push(kelompok.id);
    } else {
      const i = this.selectedKelompok.findIndex((k: number) => k === kelompok.id);
      if (i > -1) {
        this.selectedKelompok.splice(i, 1);
      }
    }
    this.groupsFilter.emit(this.selectedKelompok);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.subkelompoks) {
      $('.btn-filter-selected').remove();

      this.selectedKelompok = [];
      this.active = [];
      this.groupsFilter.emit(this.selectedKelompok);
    }
  }
}
