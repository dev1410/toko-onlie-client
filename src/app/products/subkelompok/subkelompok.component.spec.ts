import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubkelompokComponent } from './subkelompok.component';

describe('SubkelompokComponent', () => {
  let component: SubkelompokComponent;
  let fixture: ComponentFixture<SubkelompokComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubkelompokComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubkelompokComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
