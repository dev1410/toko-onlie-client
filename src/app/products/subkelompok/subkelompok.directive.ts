import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appSubkelompok]'
})
export class SubkelompokDirective {

  constructor(public viewContainerRef: ViewContainerRef = null) { }

}
