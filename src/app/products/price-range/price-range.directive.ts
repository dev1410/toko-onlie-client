import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appPriceRange]'
})
export class PriceRangeDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
