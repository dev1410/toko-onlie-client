import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormControl} from '@angular/forms';

declare var $: any;

@Component({
  selector: 'app-price-range',
  templateUrl: './price-range.component.html',
  styleUrls: ['./price-range.component.css']
})
export class PriceRangeComponent implements OnInit, OnDestroy {

  @Input() max_range: number;
  @Output() priceRange: EventEmitter<string> = new EventEmitter<string>(true);

  range = new FormControl('');

  console = null;
  timeout = null;

  constructor() { }

  ngOnInit(): void {
    this.range.setValue(`0-${this.max_range}`);
    const max_range = this.max_range;
    const range = this.range;
    const rangeEmitter = this.priceRange;
    let timeout = null;

    $( function() {
      $( '#slider-range' ).slider({
        range: true,
        min: 0,
        max: max_range,
        values: [ 0, max_range ],
        slide: function( event, ui ) {
          const min = (ui.values[0] + '').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
          const max = (ui.values[1] + '').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
          $('.range_min').text(min);
          $('.range_max').text(max);

          clearTimeout(timeout);
          timeout = setTimeout(() => {
            range.setValue(`${min.replace(',', '')}-${max.replace(',', '')}`);
            rangeEmitter.emit(range.value);
          }, 500);
        }
      });
    });
  }
  ngOnDestroy(): void {
    const minValue = $('#slider-range').attr('min');
    const maxValue = $('#slider-range').attr('max');
    const minPrice = this.formatHarga(minValue);
    const maxPrice = this.formatHarga(maxValue);
    $('.rangeslider input:eq(0)').val(minValue);
    $('.rangeslider input:eq(1)').val(maxValue);
    $('.range_min').text(minPrice);
    $('.range_max').text(maxPrice);
    $('.ui-slider-range').attr('style', 'left: 0%; width: 96.0955%;');
    $('.ui-slider-handle:eq(0)').attr('style', 'left: 0%;');
    $('.ui-slider-handle:eq(1)').attr('style', 'left: 100%;');
  }

  private formatHarga(price: number) {
    return (price + '').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
  }
}
