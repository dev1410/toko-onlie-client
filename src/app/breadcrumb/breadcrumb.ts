export interface Breadcrumb {
  gender: string;
  subcategory: string;
  group: string;
  product: string;
  product_name: string;
}
