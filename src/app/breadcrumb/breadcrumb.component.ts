import {Component, Input} from '@angular/core';
import {Breadcrumb} from './breadcrumb';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css']
})
export class BreadcrumbComponent implements Breadcrumb {

  @Input() gender;
  @Input() subcategory;
  @Input() group;
  @Input() product;
  @Input() product_name;
}
