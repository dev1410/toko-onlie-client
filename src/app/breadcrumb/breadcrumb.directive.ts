import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[app-breadcrumb]',
})
export class BreadcrumbDirective {
  constructor(public viewContainerRef: ViewContainerRef) {}
}
