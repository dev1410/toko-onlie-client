import {Component, OnDestroy, OnInit} from '@angular/core';
import {RouterService} from '../router.service';
import {Title} from '@angular/platform-browser';
import {TitleCasePipe} from '@angular/common';
import {ActivatedRoute} from '@angular/router';
import {DepartmentService} from '../shared/services/department.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  gender: string;

  constructor(private route: ActivatedRoute,
              private routerService: RouterService,
              private titleService: Title,
              private ds: DepartmentService) {
  }

  ngOnInit() {
    this.gender = this.route.snapshot.url[0].path;
    this.ds.setSubdepartments(this.gender);
    this.titleService.setTitle(`Fashion ${new TitleCasePipe().transform(this.gender)}`);
  }

  ngOnDestroy(): void {
  }
}
