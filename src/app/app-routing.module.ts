import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {ProductFilterComponent} from './products/product-filter/product-filter.component';
import {ProductDetailComponent} from './products/product-detail/product-detail.component';
import {CartComponent} from './cart/cart.component';
import {CheckoutComponent} from './cart/checkout/checkout.component';
import {AuthGuard} from './user/auth/auth.guard';
import {WishlistComponent} from './wishlist/wishlist.component';
import {CheckoutGuard} from './cart/checkout/checkout.guard';
import {CheckoutSuccessComponent} from './cart/checkout-success/checkout-success.component';
import {GenderGuard} from './shared/guards/gender.guard';
import {ProductSearchComponent} from './products/product-search/product-search.component';
import {StartkitComponent} from './startkit/startkit.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';

const routes: Routes = [
  {path: '', redirectTo: '/wanita', pathMatch: 'full'},
  {path: 'wanita', component: HomeComponent, pathMatch: 'full'},
  {path: 'wanita/:subcategory', component: ProductFilterComponent, pathMatch: 'full', canActivate: [GenderGuard]},
  {path: 'wanita/:subcategory/:group', component: ProductFilterComponent, pathMatch: 'full', canActivate: [GenderGuard]},
  {path: 'wanita/:subcategory/:group/:product', component: ProductDetailComponent, pathMatch: 'full', canActivate: [GenderGuard]},

  {path: 'pria', component: HomeComponent, pathMatch: 'full'},
  {path: 'pria/:subcategory', component: ProductFilterComponent, pathMatch: 'full', canActivate: [GenderGuard]},
  {path: 'pria/:subcategory/:group', component: ProductFilterComponent, pathMatch: 'full', canActivate: [GenderGuard]},
  {path: 'pria/:subcategory/:group/:product', component: ProductDetailComponent, pathMatch: 'full', canActivate: [GenderGuard]},
  {path: 'product/:slug', component: ProductDetailComponent},

  {path: 'pengguna', loadChildren: './user/user.module#UserModule'},
  {path: 'cart', component: CartComponent, canActivate: [GenderGuard]},
  {path: 'cart/confirm', component: CheckoutComponent, canActivate: [AuthGuard, CheckoutGuard, GenderGuard]},
  {path: 'cart/purchased', component: CheckoutSuccessComponent, canActivate: [AuthGuard, GenderGuard]},
  {path: 'wishlist', component: WishlistComponent, canActivate: [AuthGuard, GenderGuard]},
  {path: 'pencarian', component: ProductSearchComponent},
  {path: 'page/:slug', component: StartkitComponent},

  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(
    routes,
    {
      scrollPositionRestoration: 'enabled'
    })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
