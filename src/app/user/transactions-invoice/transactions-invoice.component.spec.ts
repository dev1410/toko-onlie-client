import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionsInvoceComponent } from './transactions-invoice.component';

describe('TransactionsInvoceComponent', () => {
  let component: TransactionsInvoceComponent;
  let fixture: ComponentFixture<TransactionsInvoceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionsInvoceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionsInvoceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
