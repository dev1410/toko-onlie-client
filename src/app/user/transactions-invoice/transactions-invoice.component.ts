import {Component, OnDestroy, OnInit, Input} from '@angular/core';
import {HttpWrapper} from '../../_services/http-wrapper';
import { ActivatedRoute } from '@angular/router';
import {Title} from '@angular/platform-browser';
import {Transaction} from './transaction';

var api: HttpWrapper;

@Component({
  selector: 'app-transactions-invoice',
  templateUrl: './transactions-invoice.component.html',
  styleUrls: ['./transactions-invoice.component.css']
})
export class TransactionsInvoceComponent implements OnInit, OnDestroy {

    invoice: Transaction;
    id : string = "0";

    constructor(private http: HttpWrapper, private route: ActivatedRoute, private titleService: Title) {
        api = http;
        this.id = this.route.snapshot.paramMap.get("id");
    }

    ngOnInit() {
        api.get<any>('/order/invoice/'+this.id, {}).subscribe((res: any) => {
            this.invoice = res;
            this.titleService.setTitle(`#${res.invoice}`);
            // console.log(JSON.stringify(res, null, 4));
        });
    }

    ngOnDestroy() {
    }
}
