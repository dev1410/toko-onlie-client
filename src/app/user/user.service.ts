import {EventEmitter, Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {User} from './model/user';

const httpOptions = {
  headers: new HttpHeaders({
    'Accept':  'application/json',
    'Content-Type':  'application/x-www-form-urlencoded',
  })
};
 

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private token: string;
  private tokenExpiry: string;
  private refreshToken: string;
  user: User;
  isLoggedIn$: EventEmitter<boolean> = new EventEmitter(true);

  private API_URL = environment.apiUrl;
  constructor(private http: HttpClient) {
    const token = localStorage.getItem('__csrf');
    const tokenExpiry = localStorage.getItem('__csrfExpireDate');

    const refreshToken = localStorage.getItem('__refresh');

    if (token) {
      this.token = token;

      this.isLoggedIn$.emit(true);
    }

    if (refreshToken) {
      this.refreshToken = refreshToken;

      const expiredDate = this.expiryDateTime(tokenExpiry);
      const now = new Date();

      if (now > expiredDate) {
        this.getNewToken();
      }
    }
  }

  expiryDateTime(value: string) {
    const split = value.split(' ');
    const date_split = split[0].split('-');
    const time_split = split[1].split(':');

    return new Date(
      +date_split[0], +date_split[1], +date_split[2],
      +time_split[0], +time_split[1], +time_split[2]);
  }

  getNewToken() {
    const endpoint = `${this.API_URL}/refresh/token`;

    return this.http.post<any>(endpoint, {'refresh_token': this.refreshToken}, httpOptions)
      .pipe(
        catchError(this.handleError<any>(`Refresh Token Failed`))
      )
      .subscribe(httpResponse => {
        if (httpResponse.code === 1) {
          this.setToken(httpResponse.data.access_token,
            httpResponse.data.expired_token,
            httpResponse.data.refresh_token);
        }
      });
  }

  login(user: User) {
    const endpoint = `${this.API_URL}/login`;
    const data = {
      'email': user.email,
      'password': user.password
    };

    return this.http.post<any>(endpoint, data)
      .pipe(
        catchError(this.handleError<any>(`post login ${user.email}`))
      );
  }

  logout(): void {
    localStorage.removeItem('__csrf');
    localStorage.removeItem('__csrfExpireDate');
    localStorage.removeItem('cartItems');
    localStorage.removeItem('__refresh');
    localStorage.removeItem('confirmedItems');
    this.isLoggedIn$.emit(false);
  }

  setToken(token: string, expireDate: string, refreshToken: string = null) {
    this.token = token;
    this.tokenExpiry = expireDate;
    localStorage.setItem('__csrf', token);
    localStorage.setItem('__csrfExpireDate', expireDate);

    if (refreshToken) {
      this.refreshToken = refreshToken;
      localStorage.setItem('__refresh', this.refreshToken);
    }
  }

  getToken(): string | null {
    if (this.token) {
      return this.token;
    }
    return null;
  }

  register(user: User) {

    const endpoint = `${this.API_URL}/register`;

    const formData = new FormData();
    formData.append('email', user.email);
    formData.append('password', user.password);
    formData.append('password_confirm', user.password_confirm);

    return this.http.post<any>(endpoint, formData)
      .pipe(
        catchError(this.handleError<any>(`POST REGISTER ${user}`))
      );
  }

  resetPassword(email: string, url: string) {
    if (email && url) {

      const endpoint = `${this.API_URL}/password/reset`;
      return this.http.post(endpoint, {email: email, link: url}).pipe(
        catchError(this.handleError<any>(`POST RESET PASSWORD ${email}`))
      );
    }
    return;
  }

  resetPasswordVerify(verification_code: number, email: string) {
    const endpoint = `${this.API_URL}/password/reset/confirm`;
    return this.http.post(endpoint, {email: email, code_password_reset: verification_code}).pipe(
      catchError(this.handleError<any>(`POST RESET PASSWORD ${email}`))
    );
  }

  resetPasswordConfirm(email: string, password: string, password_confirm: string, token: string) {
    const endpoint = `${this.API_URL}/password/reset/process`;
    return this.http.post(endpoint,
      {email: email, password: password, password_confirm: password_confirm},
      {headers: new HttpHeaders({'Authorization': `Bearer ${token}`})})
      .pipe(catchError(this.handleError<any>(`POST RESET PASSWORD ${email}`))
    );
  }

  isLoggedIn(): boolean {
    return !!this.token;
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: better job of transforming error for user consumption
      // console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result
      return of(result as T);

    };
  }

}
