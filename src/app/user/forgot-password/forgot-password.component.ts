import {Component, OnInit} from '@angular/core';
import {UserService} from '../user.service';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  DOMAIN: string;
  textTitle = 'Lupa Password ?';
  isSubmitted: boolean;
  isSubmitFail: boolean;

  constructor(private userService: UserService) { }

  email = new FormControl('', Validators.email);

  ngOnInit() {
    this.DOMAIN = location.hostname;
  }

  reset(): void {
    if (this.email.valid) {
      const url = `${this.DOMAIN}/pengguna/password/confirm`;
      this.userService.resetPassword(this.email.value, url)
        .subscribe(
          (next: any) => {
            if (next.code === 1) {
              this.textTitle = 'Silahkan cek email anda';
              this.isSubmitted = true;
            } else {
              if (next.code === 2) {
                this.textTitle = 'Email anda belum terdaftar';
              }
              this.isSubmitFail = true;
            }
          });
    }
  }

}
