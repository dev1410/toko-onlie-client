import {Component, OnInit, ViewChild} from '@angular/core';
import {UserService} from '../user.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {passwordMatchValidator} from '../../shared/validators/password-match.directive';
import {Router} from '@angular/router';
import {SwalComponent} from '@toverux/ngx-sweetalert2';

@Component({
  selector: 'app-forgot-password-confirm',
  templateUrl: './forgot-password-confirm.component.html',
  styleUrls: ['./forgot-password-confirm.component.css']
})
export class ForgotPasswordConfirmComponent implements OnInit {
  textTitle = 'Reset Password';
  isSuccess: boolean;

  @ViewChild('toastBerhasil') toastBerhasil: SwalComponent;

  constructor(private userService: UserService,
              private router: Router) { }

  passwordForm  = new FormGroup({
    email: new FormControl(localStorage.getItem('__emailReset')),
    password: new FormControl('', [Validators.required, Validators.minLength(6)]),
    password_confirm: new FormControl('', Validators.required),
  }, {validators: passwordMatchValidator});

  ngOnInit() {
  }

  reset() {
    const data = this.passwordForm.value;
    const token = localStorage.getItem('__resetToken');
    this.userService.resetPasswordConfirm(data.email, data.password, data.password_confirm, token)
      .subscribe((_next: any) => {
        if (_next.code === 1) {
          this.textTitle = 'Berhasil mengatur ulang password';
          localStorage.removeItem('__emailReset');
          localStorage.removeItem('__resetToken');
          this.toastBerhasil.show();
          const router = this.router;

          setTimeout(function () {
            router.navigate(['/pengguna/akun/masuk']);
          }, 3000);
        }
      });
  }

}
