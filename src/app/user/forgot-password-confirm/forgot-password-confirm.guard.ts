import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {UserService} from '../user.service';

@Injectable({
  providedIn: 'root'
})
export class ForgotPasswordConfirmGuard implements CanActivate {
  isValid = false;
  constructor(private userService: UserService,
              private router: Router) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const email = next.paramMap.get('email');
    const code = next.paramMap.get('verification');
    if (code && email) {
      this.userService.resetPasswordVerify(+code, email)
        .subscribe((_next: any) => {
          if (_next.code === 1) {
            this.isValid = true;
            localStorage.setItem('__resetToken', _next.data.access_token);
            localStorage.setItem('__emailReset', email);
            this.router.navigate(['/pengguna/password/change']);
          }
        });

      return this.isValid;
    }
    this.router.navigate(['/']);
    return false;
  }
}
