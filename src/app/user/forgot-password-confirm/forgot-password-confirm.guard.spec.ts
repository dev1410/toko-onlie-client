import { TestBed, async, inject } from '@angular/core/testing';

import { ForgotPasswordConfirmGuard } from './forgot-password-confirm.guard';

describe('ForgotPasswordConfirmGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ForgotPasswordConfirmGuard]
    });
  });

  it('should ...', inject([ForgotPasswordConfirmGuard], (guard: ForgotPasswordConfirmGuard) => {
    expect(guard).toBeTruthy();
  }));
});
