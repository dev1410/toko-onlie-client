import {Component, OnDestroy, OnInit, Input} from '@angular/core';
import {UserService} from '../user.service';
import {User} from '../model/user';
import {takeUntil} from 'rxjs/operators';
import {componentDestroyed} from '@w11k/ngx-componentdestroyed';
import {ProfileService} from '../profile/profile.service';
import {HttpWrapper} from '../../_services/http-wrapper';
import * as moment from 'moment';
import {Title} from '@angular/platform-browser';
import {Transaction} from '../transactions-invoice/transaction';
import {Productservice} from '../../products/services/product.service';
import {Router} from '@angular/router';

var api: HttpWrapper;
declare var $: any;

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css'],
  styles: [
      '.picker-icon-left,.picker-icon-right {margin-top: 6px}'
  ]
})
export class TransactionsComponent implements OnInit, OnDestroy {
  user: User;
  _tab: string;

  items = [];

  statuses: Object = {
      "0": "Menunggu Pembayaran",
      "1": "Sedang Diproses",
      "2": "Dalam Pengiriman",
      "3": "Selesai",
      "7": "Kadaluarsa"
  };

  filterEnabled: boolean = true;

  isLoading: boolean = true;

  pages = [];

  currentPage: number = 1;
  totalPages: number = 1;

  query: string;

  _query: string;

  startDate: string;

  endDate: string;

  selected: Transaction;
  private gender: string;

  clickedProduct: any;

  constructor(private userService: UserService,
              private profileService: ProfileService,
              private http: HttpWrapper,
              private titleService: Title,
              private productService: Productservice,
              private router: Router) {
    api = http;
    this.gender = localStorage.getItem('__gdr') === '1' ? 'pria' : 'wanita';
  }

  cb(start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      this.startDate = start.format('YYYY-MM-DD');
      this.endDate = end.format('YYYY-MM-DD');
      this.reload(this.tab);
  }

  ngOnInit() {
    const start = moment().subtract(31, 'days');
    const end = moment();
    const elRange = '#reportrange';
    const onChange = (date: any) => {
      this.onDateChange(date);
    };

    $(elRange).daterangepicker({
      startDate: start,
      endDate: end,
      locale: { format: 'DD/MM/YYYY' },
      ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      }
    }, this.cb(start, end));

    this.cb(start, end);

    $(elRange).on('apply.daterangepicker', function(ev, picker) {
      const date = {
        start: picker.startDate,
        end: picker.endDate
      };

      onChange(date);
    });

      this.titleService.setTitle('Akun - Detail Transaksi');
      this.getProfile();
  }

  onDateChange(date: any) {
      this.cb(date.start, date.end);
  }

  @Input()
  set tab(tab: string) {
      this._tab = tab;
      this.isLoading = true;
      if (tab) {
          this.reload(tab);
      } else {
          this.isLoading = false;
      }
  }

  doSearch() {
      this.query = this._query;
      this.reload(this.tab);
  }

  reload(tab: string) {
      this.isLoading = true;
      let from = this.startDate;
      let to = this.endDate;
      let data = new FormData();
      data.append("from", from);
      data.append("to", to);
      if (this.query) {
          data.append("search", this.query);
      }
      this.items = [];
      api.post<any>("/order/history/"+tab+"?page="+this.currentPage, data).subscribe((res : any) => {
          this.items = res.data.map(item => {
            const epx_date = item.expired_time.split(' ');
            epx_date[0] = epx_date[0].replace(/:/g, '-');
            item.expired_time = epx_date.join(' ');
            return item;
          });
          this.isLoading = false;
          this.totalPages = res.last_page;
          this.createPaginationFrom();
      });
  }

  get tab(): string {
      return this._tab;
  }

  gotoPage(pageNumber) {
      this.currentPage = pageNumber > 0 ? pageNumber : 1;
      if (this.currentPage > this.totalPages) {
          this.currentPage = this.totalPages;
      }
      this.reload(this.tab);
  }

  getProfile(): void {
    this.profileService.getUser()
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe((user: User) => {
          this.user = user;
          var start = moment().subtract(31, 'days');
          var end = moment();
          this.startDate = start.format("YYYY-MM-DD");
          this.endDate = end.format("YYYY-MM-DD");
          this.tab = "all";
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      });
  }

  createPaginationFrom() {
      this.pages = [];
      let max = 5;
      let pageSize = max;
      let totalItems = pageSize * 10;
      if (this.totalPages >= 1) {
          if (this.currentPage < 1) {
              this.currentPage = 1;
          } else if (this.currentPage > this.totalPages) {
              this.currentPage = this.totalPages;
          }

          let startPage: number, endPage: number;
          if (this.totalPages <= 5) {
              startPage = 1;
              endPage = this.totalPages;
          } else {
              if (this.currentPage <= 2) {
                  startPage = 1;
                  endPage = max;
              } else if (this.currentPage + 2 >= this.totalPages) {
                  startPage = this.totalPages - 4;
                  endPage = this.totalPages;
              } else {
                  startPage = this.currentPage - 2;
                  endPage = this.currentPage + 2;
              }
          }

          let startIndex = (this.currentPage - 1) * pageSize;
          let endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

          for (var i = startPage; i <= endPage; i++) {
              this.pages.push(i);
          }
      }
  }

  ngOnDestroy(): void {
  }

  openModal(item: any) {
      this.selected = item;
  }

  goToProduct(product_id: number) {
    this.productService.getProductsByIdList([product_id])
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe(
        (response: any) => {
          this.clickedProduct = response.data[0];
          },
        () => {},
        () => {
          $('#modalDetailsOrder').modal('hide');
          this.router.navigate(
            ['/', this.gender,
              this.clickedProduct.sub_department_slug,
              this.clickedProduct.kelompok_slug,
              this.clickedProduct.product_slug]);
        });
  }

}
