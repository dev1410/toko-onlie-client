export class User {
  id: number;
  email: string;
  password: string;
  password_confirm: string;
  name: string;
  last_name: string;
  hp: string;
  birthday: string;
  is_logged_in: boolean;
  gender_id: number;
  gender: string;
  avatar: string;
}
