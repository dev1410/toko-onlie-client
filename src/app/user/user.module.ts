import { NgModule } from '@angular/core';
import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ForgotPasswordConfirmComponent } from './forgot-password-confirm/forgot-password-confirm.component';
import {PasswordStrengthMeterModule} from 'angular-password-strength-meter';
import { TransactionsComponent } from './transactions/transactions.component';
import { TransactionsInvoceComponent } from './transactions-invoice/transactions-invoice.component';
import { ProfileComponent } from './profile/profile.component';
import { AccountNavigationComponent } from './account-navigation/account-navigation.component';
import {GlobalModule} from '../shared/global/global.module';



@NgModule({
  declarations: [
    UserComponent,
    ForgotPasswordComponent,
    ForgotPasswordConfirmComponent,
    TransactionsComponent,
    TransactionsInvoceComponent,
    ProfileComponent,
    AccountNavigationComponent,
  ],
  imports: [
    GlobalModule,
    PasswordStrengthMeterModule,
    UserRoutingModule
  ]
})
export class UserModule { }
