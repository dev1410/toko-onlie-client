import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UserComponent} from './user.component';
import {ForgotPasswordComponent} from './forgot-password/forgot-password.component';
import {ForgotPasswordConfirmGuard} from './forgot-password-confirm/forgot-password-confirm.guard';
import {AuthGuard} from './auth/auth.guard';
import {ForgotPasswordConfirmComponent} from './forgot-password-confirm/forgot-password-confirm.component';
import {TransactionsComponent} from './transactions/transactions.component';
import {TransactionsInvoceComponent} from './transactions-invoice/transactions-invoice.component';
import {ProfileComponent} from './profile/profile.component';
import {ForgotPasswordGuard} from './forgot-password.guard';

const userRoutes: Routes = [
  {
    path: '',
    component: UserComponent,
    children: [
      {path: 'akun', loadChildren: './auth/auth.module#AuthModule'},
      {path: 'transactions', component: TransactionsComponent, canActivate: [AuthGuard]},
      {path: 'transactions/invoice/:id', component: TransactionsInvoceComponent, canActivate: [AuthGuard]},
      {path: 'reset-password', component: ForgotPasswordComponent},
      {path: 'password/change', component: ForgotPasswordConfirmComponent, canActivate: [ForgotPasswordGuard]},
      {path: 'password/confirm/:verification/:email', component: ForgotPasswordConfirmComponent, canActivate: [ForgotPasswordConfirmGuard]},
      {path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]},
    ],
  },
  ];

@NgModule({
  imports: [RouterModule.forChild(userRoutes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
