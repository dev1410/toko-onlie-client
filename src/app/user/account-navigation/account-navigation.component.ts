import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-account-navigation',
  templateUrl: './account-navigation.component.html',
  styleUrls: ['./account-navigation.component.css']
})
export class AccountNavigationComponent implements OnInit {

  @Input() user;

  constructor() { }

  ngOnInit() {
  }

}
