import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { ProfileService } from './profile.service';
import {takeUntil} from 'rxjs/operators';
import {componentDestroyed} from '@w11k/ngx-componentdestroyed';
import {User} from '../model/user';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {passwordMatchValidator} from '../../shared/validators/password-match.directive';
import {Title} from '@angular/platform-browser';
import {AddressService} from '../../shared/services/address.service';
import {Address} from '../../shared/models/address';
import {Observable} from 'rxjs';
import {AddressSuggestion} from '../../shared/models/address-suggestion';
import {SwalComponent} from '@toverux/ngx-sweetalert2';

declare var $: any;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit, OnDestroy {

  @ViewChild('toastAddressDeleted') toastAddressDeleted: SwalComponent;
  @ViewChild('toastAddressAdded') toastAddressAdded: SwalComponent;
  @ViewChild('toastAddressSetAsPrimary') toastAddressSetAsPrimary: SwalComponent;
  @ViewChild('toastUpdateProfile') toastUpdateProfile: SwalComponent;
  @ViewChild('toastUpdateAvatar') toastUpdateAvatar: SwalComponent;

    public user: User;

    stateProfile = false;
    stateSecurity = false;
    stateAddress = false;

    changePasswordSuccess: boolean;

    addresses: Address[];
    modalTitle: string;
    editedAddress: Address;

    securityForm  = new FormGroup({
      old_password: new FormControl('', Validators.required),
      password: new FormControl('', [Validators.required, Validators.minLength(6)]),
      password_confirm: new FormControl('', Validators.required),
    }, {validators: passwordMatchValidator});

    addressForm = new FormGroup({
      address_id: new FormControl(''),
      address_name: new FormControl('', [Validators.required, Validators.minLength(3)]),
      receiver_name: new FormControl('', [Validators.required, Validators.minLength(3)]),
      hp: new FormControl('', [Validators.required, Validators.minLength(8)]),
      district_id: new FormControl('', Validators.required),
      address: new FormControl('', [Validators.required, Validators.minLength(6)]),
      postal_code: new FormControl(''),
      is_primary: new FormControl(''),
    });
    addressDistrict = new FormControl('');
    addressSuggestions$: Observable<AddressSuggestion[]>;
    private isNewAddress: boolean;
    private primaryAddress: Address;

    dayOptions = [
      '01', '02', '03', '04', '05', '06', '07', '08', '09', '10',
      '11', '12', '13', '14', '15', '16', '17', '18', '19', '20',
      '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'].map(d => {
        return {
          value: d,
          name: d
        };
    });

    monthOptions = [
      {value: '01', name: 'Jan'},
      {value: '02', name: 'Feb'},
      {value: '03', name: 'Mar'},
      {value: '04', name: 'Apr'},
      {value: '05', name: 'May'},
      {value: '06', name: 'Jun'},
      {value: '07', name: 'Jul'},
      {value: '08', name: 'Aug'},
      {value: '09', name: 'Sep'},
      {value: '10', name: 'Oct'},
      {value: '11', name: 'Nov'},
      {value: '12', name: 'Dec'},
    ];

    yearOptions = this.generateRange(1950, 2018).map(y => {
      return {name: y, value: y};
    });

    constructor(private profileService: ProfileService,
                private cd: ChangeDetectorRef,
                private titleService: Title,
                private addressService: AddressService) {
    }

    isEditName = false;
    isEditHp = false;
    isEditBirthday = false;
    isEditGender = false;
    isEditAvatar = false;
    isEditAvatarValid = true;
    loading = false;

    dayControl = new FormControl(this.dayOptions[0], [
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(2),
    ]);
    monthControl = new FormControl(this.monthOptions[0], [
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(2),
    ]);
    yearControl = new FormControl(this.yearOptions[0], [
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(4),
    ]);

    genderControl = new FormControl('', Validators.required);

    avatarControl = new FormControl('', Validators.required);

    ngOnInit() {
      this.stateProfile = true;
      this.titleService.setTitle('Akun - Data Diri');
      this.loading = true;
      this.getProfile();

      this.addressService.list()
        .pipe(takeUntil(componentDestroyed(this)))
        .subscribe(
          (next: Address[]) => {
            this.addresses = next;
            this.primaryAddress = next.filter((address: Address) => address.is_primary)[0];
      });
    }

    updateProfile() {
      this.user.hp = $('#edit-hp').text().trim();

      if (this.isEditBirthday) {
        this.user.birthday = `${this.yearControl.value.value}-${this.monthControl.value.value}-${this.dayControl.value.value}`;
      }

      if (this.isEditGender) {
        this.user.gender_id = this.genderControl.value;
      }

      this.profileService.updateProfile(this.user)
        .pipe(takeUntil(componentDestroyed(this)))
        .subscribe(
          (res: any) => {
            if (res.code === 1) {
              this.isEditName = false;
              this.isEditHp = false;
              this.isEditBirthday = false;
              this.isEditGender = false;
              this.toastUpdateProfile.show();
            }
          });
    }

    updateAvatar() {
      const formData = new FormData();
      formData.append('avatar', this.avatarControl.value[0]);
      this.profileService.updateAvatar(formData)
        .pipe(takeUntil(componentDestroyed(this)))
        .subscribe((next: any) => {
          if (next.code === 1) {
            this.isEditAvatar = false;
            this.isEditAvatarValid = true;
            this.toastUpdateAvatar.show();
          }
        });
    }

    getProfile() {
        this.profileService.getUser()
          .pipe(takeUntil(componentDestroyed(this)))
          .subscribe((user: User) => {
            if (!user) {
                return;
            }
            if (!user.hp) {
                user.hp = '. . . . . . . . . . .';
            }
            this.user = user;

            if (user.gender_id) {
              this.genderControl.setValue(user.gender_id);
            } else {
              this.isEditGender = true;
            }

            if (!user.birthday) {
              this.isEditBirthday = true;
            }

            this.loading = false;
        });
    }

    editHp(event) {
      event.stopPropagation();
      $('#edit-hp').editable('toggle');

      $('input.input-sm').on('keydown', function (e) {

        if ([46, 8, 9, 27, 13, 110, 190].indexOf(e.keyCode) !== -1 ||
          // Allow: Ctrl+A
          (e.keyCode === 65 && e.ctrlKey === true) ||
          // Allow: Ctrl+C
          (e.keyCode === 67 && e.ctrlKey === true) ||
          // Allow: Ctrl+X
          (e.keyCode === 88 && e.ctrlKey === true) ||
          // Allow: home, end, left, right
          (e.keyCode >= 35 && e.keyCode <= 39)) {
          // let it happen, don't do anything
          return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
          e.preventDefault();
        }
      });
      this.isEditHp = true;
    }

    editName() {
      this.isEditName = true;
    }

    editBirthday(): void {
      this.isEditBirthday = true;

      if (this.user.birthday) {
        const birthday = this.user.birthday;
        this.dayControl.setValue(this.dayOptions[+birthday.split('-')[2] - 1]);
        this.monthControl.setValue(this.monthOptions[+birthday.split('-')[1] - 1]);
        const y_index = this.yearOptions.findIndex(obj => {
          return obj.name === +birthday.split('-')[0];
        });
        this.yearControl.setValue(this.yearOptions[y_index]);
      }
    }

    editGender(): void {
      this.isEditGender = true;
    }

    editAvatar(): void {
      this.isEditAvatar = true;
      $('.avatar').click();

      let file;

      $(document).on('change', '.avatar', function() {

        if ((file = this.files[0])) {
          if (file.type === 'image/jpeg' || file.type === 'image/jpg' || file.type  === 'image/png' ) {
            $('.preview-change-avatar').attr('src', window.URL.createObjectURL(file) );
          }
        }
      });
    }

    addAddress(): void {
      this.addressForm.reset();
      this.modalTitle = 'Tambah Alamat Baru';
      this.isNewAddress = true;
      const headerWidth = $('#header').width();
      $('#modalAddress').modal({
        backdrop: 'static',
        keyboard: true,
        show : true
      });
      $('#header').width(headerWidth);

      this.addressDistrict.valueChanges.subscribe(
        (next: string) => {
          if (next) {
            if (next.length > 3) {
              if (next.length < 9) {
                $('.autocomplete-items').show();
                this.addressSuggestions$ = this.addressService.getSuggestion(next);
              }
            } else {
              $('.autocomplete-items').hide();
            }
          }
        });
    }

    resetForm() {
      this.isNewAddress = undefined;
      this.addressForm.reset();
      this.addressDistrict.reset();
    }

    editAddress(address: Address): void {
      this.modalTitle = 'Ubah Alamat';
      const headerWidth = $('#header').width();
      $('#modalAddress').modal({
        backdrop: 'static',
        keyboard: true,
        show : true
      });
      $('#header').width(headerWidth);

      this.editedAddress = address;

      this.addressForm.setValue({
        address_id: this.editedAddress.address_id,
        address_name: this.editedAddress.address_name,
        receiver_name: this.editedAddress.receiver_name,
        hp: this.editedAddress.hp,
        district_id: this.editedAddress.district_id,
        address: this.editedAddress.address,
        postal_code: this.editedAddress.postal_code,
        is_primary: this.editedAddress.is_primary
      });

      this.addressDistrict.setValue(`${address.province_name}, ` +
        `${address.city_name}, ${address.district_name}`);

      this.addressDistrict.valueChanges.subscribe(value => {
        if (value) {
          if (value.length > 3) {
            if (value.length < 9) {
              $('.autocomplete-items').show();
              this.addressSuggestions$ = this.addressService.getSuggestion(value);
            }
          } else {
            $('.autocomplete-items').hide();
          }
        }
      });
    }

  saveAddress() {
    let address = this.editedAddress ? this.editedAddress : new Address;

    address = this.addressForm.value;

    if (!address.is_primary) {
      address.is_primary = false;
    }

    if (this.isNewAddress) {
      this.addressService.add(address)
        .pipe(takeUntil(componentDestroyed(this)))
        .subscribe(
          (_address: Address | any) => {
            if (_address.is_primary) {
              this.primaryAddress.is_primary = false;
              this.primaryAddress = _address;
            }
            let newAddress: Address;
            newAddress = _address;
            newAddress.address_id = _address.id;
            this.addresses.push(newAddress);
          },
          error => {},
          () => {
            $('#modalAddress').modal('hide');
            this.resetForm();
          });

      this.isNewAddress = undefined;
      this.toastAddressAdded.show();
    } else {
      this.addressService.update(address)
        .pipe(takeUntil(componentDestroyed(this)))
        .subscribe( (res: Address | any) => {
          if ('code' in res) {
            // do something if failed
          } else {
            const i = this.addresses.findIndex((_address: Address) => _address.address_id === address.address_id);

            let editedAddress: Address;
            editedAddress = res;
            editedAddress.address_id = res.id;
            if (editedAddress.is_primary) {
              this.primaryAddress.is_primary = false;
              this.primaryAddress = editedAddress;
            }
            this.addresses[i] = editedAddress;
            this.editedAddress = undefined;
          }
        },
          error => {},
          () => {
            $('#modalAddress').modal('hide');
            this.resetForm();
            this.toastAddressAdded.show();
          });
      }

    this.addressDistrict.reset();
    }

    deleteAddress(address: Address): void {
      this.addressService.delete(address)
        .pipe(takeUntil(componentDestroyed(this)))
        .subscribe(
          (next: any) => {
            if (next.code === 1) {
              const i = this.addresses.findIndex((_addr: Address) => _addr.address_id === address.address_id);

              this.addresses.splice(i, 1);
            }
          },
            error => {},
          () => {
            this.toastAddressDeleted.show();
          });
    }

    setAddressAsPrimary(address: Address): void {

      if (address.is_primary) { return; }

      this.addressService.setPrimary(address)
        .pipe(takeUntil(componentDestroyed(this)))
        .subscribe((res: any) => {
          if (res.code === 1) {
            this.primaryAddress.is_primary = false;
            address.is_primary = true;

            this.primaryAddress = address;
          }
        },
          () => {},
          () => {
          this.toastAddressSetAsPrimary.show();
          });
    }

    searchAddress(value: string): void {
      if (value) {
        const regex = new RegExp(value, 'i');
        const data = $('.list-address');
        data.each(function(a, b) {
          $(b).hide();
          const isi = b['innerText'];
          if (isi.match(regex)) {
            $(b).show();
          }
        });
      } else {
        $('.list-address').show();
      }
    }

    setDistrictText(suggestion: AddressSuggestion): void {
      $('.autocomplete-items').hide();
      this.addressForm.patchValue({district_id: suggestion.district_id});
      this.addressDistrict.setValue(suggestion.name_text);
    }

    ngOnDestroy(): void {
    }

    generateRange(_from: number, _to: number): number[] {
      const range = [];
      if (_to > _from) {
        for (let i = _from; i <= _to; i++ ) {
          range.push(i);
        }
      }
      return range;
    }

    changePassword(): void {
      this.profileService.updatePassword(this.securityForm.value)
        .pipe(takeUntil(componentDestroyed(this)))
        .subscribe(
          (next: any) => {
            if (next.code === 1) {
              this.securityForm.reset();
              this.changePasswordSuccess = true;
            } else {
              this.securityForm.reset();
              this.changePasswordSuccess = false;
            }
          });
    }

    onFileChange(event) {

      const reader = new FileReader();

      if (event.target.files && event.target.files.length) {
        const [file] = event.target.files;

        if (file.type === 'image/jpeg' || file.type === 'image/jpg' || file.type  === 'image/png') {
          reader.readAsDataURL(file);
          reader.onload = () => {
            this.avatarControl.setValue(event.target.files);
            // @ts-ignore
            this.user.avatar = reader.result;
          };
          // need to run CD since file load runs outside of zone
          this.cd.markForCheck();
          this.isEditAvatarValid = true;
        } else {
          this.isEditAvatarValid = false;
        }
      }
    }

    switchTabState(stateName: string) {
      switch (stateName) {
        case 'profile': {
          this.titleService.setTitle('Akun - Data Diri');
          this.changePasswordSuccess = undefined;
          this.stateProfile = true;
          this.stateAddress = false;
          this.stateSecurity = false;
          break;
        }
        case 'security': {
          this.titleService.setTitle('Akun - Keamanan');
          this.changePasswordSuccess = undefined;
          this.stateProfile = false;
          this.stateAddress = false;
          this.stateSecurity = true;
          break;
        }
        case 'address': {
          this.titleService.setTitle('Akun - Pengaturan Alamat');
          this.changePasswordSuccess = undefined;
          this.stateProfile = false;
          this.stateAddress = true;
          this.stateSecurity = false;
          break;
        }
      }
    }
  numberOnly(e) {
    if ([46, 8, 9, 27, 13, 110, 190].indexOf(e.keyCode) !== -1 ||
      // Allow: Ctrl+A
      (e.keyCode === 65 && e.ctrlKey === true) ||
      // Allow: Ctrl+C
      (e.keyCode === 67 && e.ctrlKey === true) ||
      // Allow: Ctrl+X
      (e.keyCode === 88 && e.ctrlKey === true) ||
      // Allow: home, end, left, right
      (e.keyCode >= 35 && e.keyCode <= 39)) {
      // let it happen, don't do anything
      return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
      e.preventDefault();
    }
  }
}
