import {Injectable} from '@angular/core';
import {HttpWrapper} from '../../_services/http-wrapper';
import {User} from '../model/user';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  private DEFAULT_AVATAR = 'https://vignette.wikia.nocookie.net/disneyzomibes/images/5/5c/Unknown-avatar.jpg' +
    '/revision/latest?cb=20180310223206';

    constructor(private http: HttpWrapper) {
    }

    getApi(): HttpWrapper {
        return this.http;
    }

    getUser(): Observable<User> {
        return this.http.get<User>('/user/profile', {})
          .pipe(tap((user: User) => {
            if (!user.avatar) {
              user.avatar = this.DEFAULT_AVATAR;
            }
            if (!user.name) {
              user.name = user.email.replace(/@.*$/, '').replace(/[^a-z]+/i, ' ');
            }
          }));
    }

    updateProfile(user: User) {
      return this.http.post<User>('/user/profile/update', user);
    }

    updateAvatar(avatar: FormData) {
      return this.http.post('/user/avatar/update', avatar);
    }

    updatePassword(value: Object) {
      return this.http.post('/user/change/password', value);
    }
}
