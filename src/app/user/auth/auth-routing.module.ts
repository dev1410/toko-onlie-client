import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {AuthComponent} from './auth.component';

const authUserRoutes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      {path: 'masuk', component: LoginComponent},
      {path: 'daftar', component: RegisterComponent},
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(authUserRoutes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
