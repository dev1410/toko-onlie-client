import {Component, OnDestroy, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {FormControl, Validators} from '@angular/forms';
import {UserService} from '../../user.service';
import {User} from '../../model/user';
import {ActivatedRoute, Router} from '@angular/router';
import {takeUntil} from 'rxjs/operators';
import {componentDestroyed} from '@w11k/ngx-componentdestroyed';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit, OnDestroy {

  email = new FormControl('', Validators.email);
  password = new FormControl('', Validators.required);
  private isRememberMe = false;
  user = new User;
  next: string;
  loginFail = false;

  constructor(private titleService: Title,
              private userService: UserService,
              private route: ActivatedRoute,
              private router: Router) {
    if (localStorage.getItem('__csrf')) {
      this.router.navigate(['/wanita']);
    }
  }

  ngOnInit() {
    this.titleService.setTitle('Masuk ke akun pembeli');

    this.next = this.route.snapshot.queryParams['next'] || '/wanita';
  }

  onSubmit(): void {
    this.user.email = this.email.value;
    this.user.password = this.password.value;

    this.login();
  }

  login() {
    this.loginFail = false;
    return this.userService.login(this.user)
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe(httpResponse => {
      if (httpResponse.code === 1) {
        this.user.is_logged_in = true;

        this.userService.setToken(httpResponse.data.access_token, httpResponse.data.expired_token);

        if (this.isRememberMe) {
          this.userService.setToken(
            httpResponse.data.access_token,
            httpResponse.data.expired_token,
            httpResponse.data.refresh_token);
        }
        window.location.href = this.next;
      } else {
        this.loginFail = true;
      }
    });
  }

  ngOnDestroy(): void {
  }
}
