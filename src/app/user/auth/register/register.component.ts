import {Component, OnDestroy, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import { FormControl, FormGroup, Validators} from '@angular/forms';
import {passwordMatchValidator} from '../../../shared/validators/password-match.directive';
import {UserService} from '../../user.service';
import {User} from '../../model/user';
import {Router} from '@angular/router';
import {takeUntil} from 'rxjs/operators';
import {componentDestroyed} from '@w11k/ngx-componentdestroyed';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit, OnDestroy {
  user: User;
  isRegistered: boolean;
  registerFail = false;
  failMessage = '';
  loginSubscribe;

  constructor(private titleService: Title,
              private userService: UserService,
              private router: Router) { }

  registerForm  = new FormGroup({
    email: new FormControl('', [Validators.email, Validators.required]),
    password: new FormControl('', [Validators.required, Validators.minLength(6)]),
    password_confirm: new FormControl('', Validators.required),
  }, {validators: passwordMatchValidator});

  ngOnInit() {
    this.titleService.setTitle('Buat akun pembeli baru');
  }

  onSubmit(): void {
    this.registerFail = false;
    this.user = this.registerForm.value;

    this.register();
  }

  register() {
    return this.userService.register(this.user)
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe(httpResponse => {

        if (httpResponse.code === 1) {
          this.loginSubscribe = this.userService.login(this.user)
            .subscribe(_httpResponse => {
              if (_httpResponse.code === 1) {
                this.user.is_logged_in = true;
                this.userService.setToken(
                  _httpResponse.data.access_token,
                  _httpResponse.data.expired_token,
                  _httpResponse.data.refresh_token);
              }
              this.router.navigate(['/']);
            });
        }

        if (httpResponse.code === 4) {
          this.failMessage = 'Anda belum aktivasi email !';
          this.registerFail = true;
        }

        if (httpResponse.code === 3) {
          this.failMessage = 'Email tidak valid !';
          this.registerFail = true;
        }

        if (httpResponse.code === 5) {
          this.failMessage = 'Email sudah terdaftar !';
          this.registerFail = true;
        }
      });
  }

  ngOnDestroy(): void {
  }
}
