import { Injectable } from '@angular/core';
import {Slider} from '../slider';
import {HttpWrapper} from '../../_services/http-wrapper';

var api: HttpWrapper;

@Injectable({
  providedIn: 'root'
})
export class SliderService {

  constructor(private http: HttpWrapper) {
      api = http;
  }

  getSliders(position: string, gender: string) {
      return api.post<Slider[]>('/slider/' + position, {tag: gender});
  }

  getImage(path: string): string {
      return api.wrapImage(path);
  }
}
