import { Slider } from './slider';
const sliderText = [
  '<h3>Los Angeles</h3>\n' +
  '<p>LA is always so much fun!</p>',
  '<h3>Chicago</h3>\n' +
  '<p>Thank you, Chicago!</p>',
  '<h3>New York</h3>\n' +
  '<p>We love the Big Apple!</p>'
];

export const SLIDERS = <Slider[]> [
  {
    src: '/assets/images/img-default/slide.jpg',
    gender: 'wanita',
    text: sliderText[0]
  },
  {
    src: '/assets/images/img-default/slider-banner-1.png',
    gender: 'pria',
    text: sliderText[1]
  },
  {
    src: '/assets/images/img-default/slide.jpg',
    gender: 'wanita',
    text: sliderText[0]
  },
  {
    src: '/assets/images/img-default/slider-banner-1.png',
    gender: 'pria',
    text: sliderText[1]
  },
  {
    src: '/assets/images/img-default/slide.jpg',
    gender: 'wanita',
    text: sliderText[0]
  },
  {
    src: '/assets/images/img-default/slider-banner-1.png',
    gender: 'pria',
    text: sliderText[1]
  },
  {
    src: '/assets/images/img-default/slide.jpg',
    gender: 'wanita',
    text: sliderText[0]
  },
  {
    src: '/assets/images/img-default/slider-banner-1.png',
    gender: 'pria',
    text: sliderText[1]
  },
  {
    src: '/assets/images/img-default/slide.jpg',
    gender: 'wanita',
    text: sliderText[0]
  },
  {
    src: '/assets/images/img-default/slider-banner-1.png',
    gender: 'pria',
    text: sliderText[0]
  },
  {
    src: '/assets/images/img-default/slide.jpg',
    gender: 'wanita',
    text: sliderText[1]
  },
  {
    src: '/assets/images/img-default/slider-banner-1.png',
    gender: 'pria',
    text: sliderText[0]
  },
];

