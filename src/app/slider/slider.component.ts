import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Slider} from './slider';
import {SliderService} from './services/slider.service';
import {takeUntil} from 'rxjs/operators';
import {componentDestroyed} from '@w11k/ngx-componentdestroyed';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit, OnDestroy {
  sliders:  Slider[];

  @Input()
  gender: string;

  constructor(private sliderService: SliderService) { }

  ngOnInit() {
    this.getSliders();
  }

  ngOnDestroy(): void {
  }

  getSliders(): void {
      this.sliderService.getSliders('main', this.gender)
        .pipe(takeUntil(componentDestroyed(this)))
        .subscribe((sliders: Slider[]) => {
          this.sliders = sliders;
          this.sliders.forEach((s: Slider) => s.image = this.sliderService.getImage(s.image));
      });
  }
}
