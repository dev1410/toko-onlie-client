import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Result} from './result';
import { Observable } from 'rxjs';


const httpOptions = {
  headers: new HttpHeaders({
    'Accept':  'application/json',
    'Content-Type':  'application/x-www-form-urlencoded',
  })
};

@Injectable({
  providedIn: 'root'
})
export class StartkitService {
  private API_URL = environment.apiUrl;
  constructor(private http: HttpClient) { }

  get(slug: string):Observable<Result> {
    const endpoint = `${this.API_URL}/page/${slug}`;
    return this.http.get<Result>(endpoint);
  }
}
