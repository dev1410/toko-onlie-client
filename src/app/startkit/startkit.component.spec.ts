import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StartkitComponent } from './startkit.component';

describe('StartkitComponent', () => {
  let component: StartkitComponent;
  let fixture: ComponentFixture<StartkitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StartkitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StartkitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
