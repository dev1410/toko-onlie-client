import {Component, OnDestroy, OnInit} from '@angular/core';
import {StartkitService} from './startkit.service';
import { Result } from './result';
import { RouterService } from '../router.service';
import {Title} from '@angular/platform-browser';
import {TitleCasePipe} from '@angular/common';
import {Router} from '@angular/router';
import {takeUntil} from 'rxjs/operators';
import {componentDestroyed} from '@w11k/ngx-componentdestroyed';


@Component({
  selector: 'app-startkit',
  templateUrl: './startkit.component.html',
  styleUrls: ['./startkit.component.css'],
})
export class StartkitComponent implements OnInit, OnDestroy {
  slug: string;
  ress: Result;
  path: string;

  constructor(
    private startkitService: StartkitService,
    private routerService: RouterService,
    private titleService: Title,
    private router: Router) { }

  ngOnInit() {
    this.path = this.router.url;
    const paths = this.path.split('/');
    this.startkitService.get(paths[2])
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe( (serverResponse: Result) => {
          if ('code' in serverResponse) {
            this.routerService.redirectToHome();
          }

          this.setRess(serverResponse);
          this.titleService.setTitle(`${new TitleCasePipe().transform(this.ress.title)}`);
        }
      );
  }

  setRess(res: Result): void {
    this.ress = res;
  }

  ngOnDestroy(): void {
  }
}
