import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  color: string;
  includeSpinner: boolean;

  title = 'SIATA.ID';

  ngOnInit() {
    // inject loading bar color
    this.color = '#d21322';
    this.includeSpinner = false;
  }
}
