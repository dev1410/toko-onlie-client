import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {BannerService} from './services/banner.service';
import {SliderService} from '../slider/services/slider.service';
import {takeUntil} from 'rxjs/operators';
import {componentDestroyed} from '@w11k/ngx-componentdestroyed';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit, OnDestroy {
  banner: string;

  @Input()
  gender: string;

  constructor(private sliderService: SliderService,
              private bannerService: BannerService) { }

  ngOnInit() {
    this.getBanner();
  }

  getBanner(): void {
    this.sliderService.getSliders('banner', this.gender)
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe((res: any) => {
        if (res) {
          this.banner = this.sliderService.getImage(res.image);
        } else {
          this.banner = this.bannerService.getBanner(this.gender).src;
        }
      });
  }

  ngOnDestroy(): void {
  }
}
