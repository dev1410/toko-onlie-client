import { Injectable } from '@angular/core';
import {Banner} from '../banner';
import {BANNER} from '../mock-banner';

@Injectable({
  providedIn: 'root'
})
export class BannerService {
  constructor() { }

  getBanner(gender: string): Banner {
    return BANNER.filter((banner: Banner) => banner.gender === gender)[0];
  }
}
