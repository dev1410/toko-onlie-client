import {Component, ElementRef, HostListener, OnInit, Renderer2} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Productservice} from '../../products/services/product.service';
import {Observable} from 'rxjs';
import {ProductTile} from '../../products/product-tile';
import {Router} from '@angular/router';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {
  searchControl = new FormControl('');
  gender = +localStorage.getItem('__gdr') === 2 ? 'wanita' : 'pria';
  suggestions$: Observable<ProductTile[] | {}>;
  timeout = null;

  constructor(private productService: Productservice,
              private router: Router,
              private el: ElementRef,
              private renderer: Renderer2) { }

  @HostListener('focusout')
  onClick() {
    this.renderer.listen('document', 'click', (event) => {
      if (!this.el.nativeElement.contains(event.target)) {
        this.suggestions$ = undefined;
      }
    });
  }

  ngOnInit() {
  }

  closeSuggestion() {
    this.suggestions$ = undefined;
  }

  clearTimeOut() {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      if (this.searchControl.value && this.searchControl.value.length > 1) {
        this.suggestions$ = this.productService.getSearchSuggestions(this.searchControl.value, this.gender)
          .pipe(
            debounceTime(300),
            distinctUntilChanged());
      }
    }, 500);
  }

  search() {
    if (this.searchControl.value.length) {
      this.closeSuggestion();
      clearTimeout(this.timeout);
      this.router.navigate(['/pencarian'], {queryParams: {keyword: this.searchControl.value.replace(' ', '+').trim()}});
    }
  }
}
