import {Subdepartment} from './subdepartment';

export const SUBDEPARTMENTS =  <Subdepartment[]>[
  {
    name: 'Terbaru',
    slug: 'terbaru'
  },
  {
    name: 'Pakaian Muslim',
    slug: 'pakaian-muslim',
  },
  {
    name: 'Hijab',
    slug: 'hijab',
  },
  {
    name: 'Aksesoris',
    slug: 'aksesoris',
  },
  {
    name: 'Sepatu',
    slug: 'sepatu',
  },
  {
    name: 'Sale',
    slug: 'sale',
  },
];
