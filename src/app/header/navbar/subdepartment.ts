import {Subkelompok} from '../../products/subkelompok';

export class Subdepartment {
  id: number;
  category_id: number;
  name: string;
  image: string;
  slug: string;
  tag: string;
  groups: Subkelompok[];
}
