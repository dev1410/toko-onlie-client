import {Component, OnDestroy, OnInit} from '@angular/core';
import {DepartmentService} from '../../shared/services/department.service';
import {takeUntil} from 'rxjs/operators';
import {componentDestroyed} from '@w11k/ngx-componentdestroyed';
import {Subdepartment} from './subdepartment';
import {NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit, OnDestroy {
  gender = '...';

  constructor(public deptService: DepartmentService,
              private router: Router) { }

  ngOnInit(): void {
    this.router.events
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe(event => {
        if (event instanceof NavigationEnd) {
          const gdr = localStorage.getItem('__gdr');

          const path = this.router.url;

          const paths = path.split('/');

          if (paths[1] === 'wanita' || paths[1] === 'pria') {
            this.gender = paths[1];
          } else {
            this.gender = +gdr === 1 ? 'pria' : 'wanita';
          }
        }
      });
  }

  ngOnDestroy(): void {
  }

  goTo(subdepartment: Subdepartment): void {
    localStorage.setItem('__sdt', JSON.stringify(subdepartment));
  }
}
