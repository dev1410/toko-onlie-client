import { Injectable } from '@angular/core';
import {Subdepartment} from './subdepartment';
import {SUBDEPARTMENTS} from './mock-subdepartmens';

@Injectable({
  providedIn: 'root'
})
export class NavbarService {

  constructor() { }

  getSubdepartments(tag: string): Subdepartment[] {
    return SUBDEPARTMENTS;
  }
}
