import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserService} from '../user/user.service';
import {NavigationEnd, Router} from '@angular/router';
import {takeUntil} from 'rxjs/operators';
import {componentDestroyed} from '@w11k/ngx-componentdestroyed';
import {WishlistService} from '../wishlist/wishlist.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  constructor(public userService: UserService,
              private router: Router,
              public wishlistService: WishlistService) { }

  ngOnInit() {
    this.router.events
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe(event => {
      if (event instanceof NavigationEnd) {

        const path = this.router.url;

        const paths = path.split('/');

        if (paths[1] === 'wanita' || paths[1] === 'pria') {
          this.setGender(paths[1] === 'pria' ? '1' : '2');
        }
      }
    });
  }

  ngOnDestroy(): void {
  }

  setGender(gender_id: string) {
    localStorage.setItem('__gdr', gender_id);
  }

  isActiveGender(gender_id: string): boolean {
    return localStorage.getItem('__gdr') === gender_id;
  }
}
