export class Subscriber {
  email: string;
  is_subscribed: number;
}
