import { Component, OnInit, ViewChild } from '@angular/core';
import {SubscriberService} from './services/subscriber.service';
import { SwalPartialTargets, SwalComponent } from '@toverux/ngx-sweetalert2';


@Component({
  selector: 'app-subscriber',
  templateUrl: './subscriber.component.html',
  styleUrls: ['./subscriber.component.css']
})
export class SubscriberComponent implements OnInit {
  email: string;
  msg:string;
  type:string;
  title:string;
  status:boolean;
  @ViewChild('confirmSwal') private confirmSwal: SwalComponent;
  @ViewChild('failSwal') private failSwal: SwalComponent;
  @ViewChild('warningSwal') private warningSwal: SwalComponent;
  
  constructor(
    public readonly swalTargets: SwalPartialTargets,
    private subscriberService: SubscriberService,
  ) { }

  ngOnInit() {
  }

  subscribe(email: string): void {
    if (email) {
      this.email = email;
      this.subscriberService.subscribe(this.email)
        .subscribe((ress: any) => {
          if(ress){
            this.confirmSwal.show();
          }else{
            this.failSwal.show();
          }
        }
          
        );
    }else{
      this.warningSwal.show();
    }
  }


}
