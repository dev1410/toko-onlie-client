import { Injectable, ViewChild } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {Subscriber} from '../subscriber';
import {catchError} from 'rxjs/operators';
import {environment} from '../../../environments/environment';
import { SwalComponent } from '@toverux/ngx-sweetalert2';
const ENDPOINT = environment.apiUrl;

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
};

@Injectable({
  providedIn: 'root'
})
export class SubscriberService {
  private subscribeUrl = `${ENDPOINT}/subscribe`;
  @ViewChild('failSwal') private failSwal: SwalComponent;
  constructor(private http: HttpClient) { }

  /** POST: subscribe new email to the server */
  subscribe(email): Observable<string> {
    const body = JSON.stringify({'email': email});
    return this.http.post<string>(this.subscribeUrl, body, httpOptions)
      .pipe(
        catchError(this.errorHandler<string>('subscribe'))
      );
  }

  unSubscribe(email: Subscriber): Observable<Subscriber> {
    const url = `${this.subscribeUrl}/${email.email}`;
    return this.http.get<Subscriber>(url)
      .pipe(
        catchError(this.errorHandler<Subscriber>('subscribe'))
      );
  }

  private errorHandler<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // console.log(`${operation} failed: ${error.message} ${error.status} ${error.code} `);

      return of(result as T);
    };
  }
}
