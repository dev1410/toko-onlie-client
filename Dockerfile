FROM pivotalpa/angular-cli:latest as angular

ENV SIATA_API_HOST_PATH="backend.siata.id"
COPY . /opt/app
WORKDIR /opt/app
RUN npm --save --loglevel=error install && \
	sed -i "s|182.16.178.172/toko-online|${SIATA_API_HOST_PATH}|g" src/environments/environment.prod.ts && \
	ng build --prod --base-href /

FROM httpd:2.4-alpine as apache
RUN sed -i \
	-e 's/^#\(LoadModule .*mod_rewrite.so\)/\1/' \
	-e 's/\(AllowOverride\) None/\1 All/g' \
	/usr/local/apache2/conf/httpd.conf 
COPY --from=angular /opt/app/dist/siata-app/ /usr/local/apache2/htdocs/
RUN echo -e "<IfModule mod_rewrite.c>\n\
	RewriteEngine On\n\
    RewriteCond %{REQUEST_FILENAME} !-f\n\
    RewriteCond %{REQUEST_FILENAME} !-d\n\
    RewriteRule . index.html [L]\n\
</IfModule>\n" > /usr/local/apache2/htdocs/.htaccess

